Code Challenge for Orange Bank:
==============================================
## Microservice that will handle bank transactions.

This Application is a microservice that will handle bank transactions.

It is built using [Spring Boot](http://projects.spring.io/spring-boot/)
The microservice is auto-contained. It has been build in the App with [H2](https://www.h2database.com/html/main.html) 'in Memory Database' 
(See Database Section)

I have used [Cucumber testing tool](https://cucumber.io/) for writing 'Given When Then' Scenarios according to ATDD process
This specifications can be found in [FEATURES](src/test/resources/features)

I have used swagger for api documentation. Often it provides a tool for api execution with real time data input. No need for external tools like soapui or postman.
This tool is deployed in *<server:port>/orangebank/swagger-ui.html* , for example, in 
localhost: 
http://localhost:8080/orangebank/swagger-ui.html

## Functional considerations
 -  Can be handle transactions when accounts associated to them exists in the system
 -  Cannot exist two Accounts with same account_iban
 -  Cannot exist two Transactions with same reference
     


## Database
[H2](https://www.h2database.com/html/main.html) Memory Database provides a client tool 
that is deployed with application in *<server:port>/orangebank/h2-console*, for example, 
in localhost:
  
http://localhost:8080/orangebank/h2-console

This database is initialized with Accounts specified in [account_ibans.data](src/resources/accounts/account_ibans.data) 
file stored in accounts resource folder

The model is defined like :

ACCOUNT:
    accountIban : String (PK)
    balance : BigDecimal

TRANSACTION:
    reference : String (PK)
    date : LocalDateTime
    amount : BigDecimal
    fee : BigDecimal
    description : String
    account : Account (FK to ACCOUNT)



## Endpoints
 -  [**Create transaction**](rest-api/docs/TransactionControllerApi.md#createtransactionusingpost)
 This endpoint will receive the transaction information and store it into the system: <br>
 -- A transaction that leaves the total account balance bellow 0 is not allowed.

-  [**Search transactions**](rest-api/docs/TransactionControllerApi.md#searchtransactionsusingget)
 This endpoint searches for transactions and should be able to: * Filter by account_iban * Sort by amount
(ascending/descending): 

-  [**Transaction status**](rest-api/docs/TransactionStatusControllerApi.md#gettransactionstatususingpost)
This endpoint, based on the payload and some business rules, will return the status and additional information
for a specific transaction.

## UML Class Diagrams

To describe the types of objects in the system and the different types of relationships that exist among them, 
Some class diagrams have been creatad.They can be found in the path 'uml/class-diagrams'  ([Class 
diagrams](uml/class-diagrams)) 

## Requirements

For building and running the application you need:

- [JDK 1.8](http://www.oracle.com/technetwork/java/javase/downloads/jdk8-downloads-2133151.html)
- [Maven 3](https://maven.apache.org)

## Running the application locally

There are several ways to run application on your local machine. One way is to execute the `main` method in the `com.orangebank.codechallenge.CodechallengeApplication` class from your IDE.
Alternatively you can use the [Spring Boot Maven plugin](https://docs.spring.io/spring-boot/docs/current/reference/html/build-tool-plugins-maven-plugin.html) like so:

```shell
mvn spring-boot:run
```
## Testing Application

you can use the mvn command like so:

```shell
mvn test
```

After running the Cucumber feature files (used in *CucumberIntegrationTest), Tests results can be found in:
-  *target/create-transactions* (for Create Transaction Functionality) 
-  *target/search-transactions* (for Search Transactions Functionality) 
-  *target/transaction-status* (for Transaction Status Functionality)


## Api Rest especification

### Documentation for API Endpoints

    All URIs are relative to */orangebank*

  Class | Method | HTTP request | Description
  ------------ | ------------- | ------------- | -------------
  *TransactionControllerApi* | [**createTransactionUsingPOST**](rest-api/docs/TransactionControllerApi.md#createtransactionusingpost) | **POST** /transactions | createTransaction
  *TransactionControllerApi* | [**searchTransactionsUsingGET**](rest-api/docs/TransactionControllerApi.md#searchtransactionsusingget) | **GET** /transactions | searchTransactions
  *TransactionStatusControllerApi* | [**getTransactionStatusUsingPOST**](rest-api/docs/TransactionStatusControllerApi.md#gettransactionstatususingpost) | **POST** /transaction-status | getTransactionStatus


### Documentation For Models

 - [CreateTransactionWebRequest](rest-api/docs/CreateTransactionWebRequest.md)
 - [GetTransactionStatusWebRequest](rest-api/docs/GetTransactionStatusWebRequest.md) 
 - [TransactionStatusWebResponse](rest-api/docs/TransactionStatusWebResponse.md)
 - [TransactionWebResponse](rest-api/docs/TransactionWebResponse.md)



