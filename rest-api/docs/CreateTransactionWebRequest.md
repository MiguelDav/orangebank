# CreateTransactionWebRequest

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**reference** | **string** | The transaction unique reference number in our system. If not present, the system
will generate one  | [optional] [default to null]
**account_iban** | **string** | The IBAN number of the account where the transaction has happened. | [mandatory] 
**date** | **string** (date) | Date when the transaction took place | [optional] [default to null]
**amount** | **float** | If positive the transaction is a credit (add money) to the account. If negative it is a
debit (deduct money from the account) | [mandatory] 
**fee** | **float** | Fee that will be deducted from the amount, regardless on the amount being positive or
negative. | [optional] [default to null]
**description** | **string** | The description of the transaction | [optional] [default to null]




