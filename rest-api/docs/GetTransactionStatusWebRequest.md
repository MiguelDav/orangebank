# GetTransactionStatusWebRequest

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**reference** | **string** | The transaction reference number | [mandatory] [default to null]
**channel** | **string** | The type of the channel that is asking for the status. It can be any of these values:
CLIENT, ATM, INTERNAL | [optional] [default to null]


