# TransactionControllerApi

All URIs are relative to */orangebank*

Method | HTTP request | Description
------------- | ------------- | -------------
[**createTransactionUsingPOST**](TransactionControllerApi.md#createTransactionUsingPOST) | **POST** /transactions | createTransaction
[**searchTransactionsUsingGET**](TransactionControllerApi.md#searchTransactionsUsingGET) | **GET** /transactions | searchTransactions


## **createTransactionUsingPOST**

createTransaction

### Example

```bash
 curl -d '{"reference":"12345A","account_iban":"ES9820385778983000760236","date":"2019-07-16T16:55:42.000Z","amount":193.38,"fee":3.18,"description":"Restaurant payment"}' -X POST http://localhost:8080/orangebank/transaction -H "Content-Type: application/json"
```


### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **transaction** | [**CreateTransactionWebRequest**](CreateTransactionWebRequest.md) | transaction |

### Return type

[**TransactionWebResponse**](TransactionWebResponse.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: */*


## **searchTransactionsUsingGET**

searchTransactions

### Example
```bash
curl -get http://localhost:8080/orangebank/transactions?account_iban=XXXXXX&sort=ASC 
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **account_iban** | **string** | account_iban,  Filter by account_iban| mandatory
 **sort** | **string** | sort , Sort by amount | [optional] It can be any of these values: 
 ASC, DESC

### Return type

[**array[TransactionWebResponse]**](TransactionWebResponse.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not Applicable
 - **Accept**: */*


