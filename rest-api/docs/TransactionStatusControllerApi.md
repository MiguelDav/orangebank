# TransactionStatusControllerApi

All URIs are relative to */orangebank*

Method | HTTP request | Description
------------- | ------------- | -------------
[**getTransactionStatusUsingPOST**](TransactionStatusControllerApi.md#getTransactionStatusUsingPOST) | **POST** /transaction-status | getTransactionStatus


## **getTransactionStatusUsingPOST**

getTransactionStatus

### Example
```bash
 curl -d '{"reference":"12345A","channel":"CLIENT"}' -X POST http://localhost:8080/orangebank/transaction-status -H "Content-Type: application/json"
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **getTransactionStatusWebRequest** | [**GetTransactionStatusWebRequest**](GetTransactionStatusWebRequest.md) | getTransactionStatusWebRequest |

### Return type

[**TransactionStatusWebResponse**](TransactionStatusWebResponse.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: */*

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

