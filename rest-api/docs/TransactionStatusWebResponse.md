# TransactionStatusWebResponse

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**reference** | **string** | The transaction reference number | [optional] [default to null]
**status** | **string** | The status of the transaction. It can be any of these values: PENDING, SETTLED, FUTURE,
INVALID | [optional] [default to null]
**amount** | **float** | the amount of the transaction | [optional] [default to null]
**fee** | **float** | The fee applied to the transaction | [optional] [default to null]




