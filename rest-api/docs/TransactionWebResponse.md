# TransactionWebResponse

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**account_iban** | **string** | The IBAN number of the account | [optional] [default to null]
**amount** | **float** | the amount of the transaction | [optional] [default to null]
**date** | **string** |Date when the transaction took place | [optional] [default to null]
**description** | **string** | The description of the transaction | [optional] [default to null]
**fee** | **float** | The fee applied to the transaction | [optional] [default to null]
**reference** | **string** | The transaction reference number | [optional] [default to null]
