package com.orangebank.codechallenge;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.util.stream.Stream;

import org.springframework.beans.factory.InitializingBean;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;

import com.orangebank.codechallenge.model.persistence.Account;
import com.orangebank.codechallenge.repository.AccountRepository;

@SpringBootApplication
public class CodechallengeApplication{

    private static final String ACCOUNTS_ACCOUNT_IBANS_DATA_FILE = "/accounts/account_ibans.data";
    @Autowired
    private AccountRepository accountRepository;

    public static void main(final String[] args){
        SpringApplication.run(CodechallengeApplication.class, args);
    }

    /**
     * Initialize database with accounts specified in accounts/account_ibans.data
     * @return
     * @throws IOException
     */
    @Bean
    InitializingBean initalizeDatabase(){
        return () -> {
            final File file = new File(this.getClass().getResource(ACCOUNTS_ACCOUNT_IBANS_DATA_FILE).getFile());
            if(file.exists()){
                try(Stream<String> input = Files.lines(file.toPath())){
                    input.map(java.lang.String::trim).filter(s -> !s.isEmpty())
                    .forEach(iban -> accountRepository.save(new Account(iban)));
                }
            }
        };
    }

}
