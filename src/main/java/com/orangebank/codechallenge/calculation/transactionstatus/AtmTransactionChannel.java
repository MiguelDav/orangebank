package com.orangebank.codechallenge.calculation.transactionstatus;

import org.springframework.stereotype.Component;

import com.orangebank.codechallenge.model.ChannelEnum;
import com.orangebank.codechallenge.model.TransactionStatusEnum;

@Component
public class AtmTransactionChannel extends TransactionChannelPrototype{

    public AtmTransactionChannel(){
        super(ChannelEnum.ATM);
    }

    @Override
    protected TransactionStatusEnum getStatusWhenTransactionDateIsGreaterThanToday(){
        return TransactionStatusEnum.PENDING;
    }


}
