package com.orangebank.codechallenge.calculation.transactionstatus;

import org.springframework.stereotype.Component;

import com.orangebank.codechallenge.model.ChannelEnum;

@Component
public class ClientTransactionChannel extends TransactionChannelPrototype{

    public ClientTransactionChannel(){
        super(ChannelEnum.CLIENT);
    }


}
