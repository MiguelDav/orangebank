package com.orangebank.codechallenge.calculation.transactionstatus;

import org.springframework.stereotype.Component;

import com.orangebank.codechallenge.model.ChannelEnum;

@Component
public class InternalTransactionChannel extends TransactionChannelPrototype{

    public InternalTransactionChannel(){
        super(ChannelEnum.INTERNAL);
    }

    @Override
    public boolean subtractFeesInAmount(){
        return false;
    }

}
