package com.orangebank.codechallenge.calculation.transactionstatus;

import java.time.LocalDateTime;

import org.springframework.beans.factory.annotation.Autowired;

import com.orangebank.codechallenge.model.ChannelEnum;
import com.orangebank.codechallenge.model.TransactionStatusEnum;
import com.orangebank.codechallenge.utils.LocalDateTimeComparationUtils;

/**
 * Transaction Channel Prototype.
 * By default:
 *      Channels with transaction date is Before today --> Status is SETTLED
 *      Channels with transaction date is Equals today --> Status is PENDING
 *      Channels with transaction date is Greater than today --> Status is FUTURE
 *      The Amount must be represented subtracting the fee
 *      
 *
 */
public abstract class TransactionChannelPrototype{

    private final ChannelEnum channel;

    @Autowired
    LocalDateTimeComparationUtils dateTimeUtils;

    protected TransactionChannelPrototype(final ChannelEnum channel){
        super();
        this.channel = channel;
    }

    public ChannelEnum getChannel(){
        return channel;
    }


    public TransactionStatusEnum getStatusWithDateTime(final LocalDateTime dateTime){
        if(dateTimeUtils.isBeforeToday(dateTime)){
            return getStatusWhenTransactionDateIsBeforeToday();
        }else{
            if(dateTimeUtils.isEqualsToToday(dateTime)){
                return getStatusWhenTransactionDateIsEqualsToToday();
            }else{
                return getStatusWhenTransactionDateIsGreaterThanToday();
            }
        }
    }

    protected TransactionStatusEnum getStatusWhenTransactionDateIsBeforeToday(){
        return TransactionStatusEnum.SETTLED;
    }

    protected TransactionStatusEnum getStatusWhenTransactionDateIsEqualsToToday(){
        return TransactionStatusEnum.PENDING;
    }

    protected TransactionStatusEnum getStatusWhenTransactionDateIsGreaterThanToday(){
        return TransactionStatusEnum.FUTURE;
    }

    public boolean subtractFeesInAmount(){
        return true;
    }


}
