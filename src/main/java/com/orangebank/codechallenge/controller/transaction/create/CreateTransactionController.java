package com.orangebank.codechallenge.controller.transaction.create;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.convert.converter.Converter;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.server.ResponseStatusException;

import com.orangebank.codechallenge.exceptions.AccountNotExistsException;
import com.orangebank.codechallenge.exceptions.DuplicateTransactionException;
import com.orangebank.codechallenge.exceptions.TransactionNotAllowedException;
import com.orangebank.codechallenge.model.persistence.Transaction;
import com.orangebank.codechallenge.service.CreateTransactionService;
import com.orangebank.codechallenge.web.model.CreateTransactionWebRequest;
import com.orangebank.codechallenge.web.model.TransactionWebResponse;

@RestController
public class CreateTransactionController{

    @Autowired
    CreateTransactionService createTransactionService;

    @Autowired
    Converter<CreateTransactionWebRequest, Transaction> transactionWebRequestIntoTransactionConverter;

    @Autowired
    Converter<Transaction, TransactionWebResponse> transactionIntoTransactionWebResponseConverter;


    @PostMapping("/transactions")
    public TransactionWebResponse createTransaction(@RequestBody
            final CreateTransactionWebRequest transaction){
        try{
            return transactionIntoTransactionWebResponseConverter.convert(createTransactionService
                    .createTransaction(transactionWebRequestIntoTransactionConverter.convert(transaction)));
        }catch(final AccountNotExistsException accountNotExistsException){
            throw new ResponseStatusException(HttpStatus.PRECONDITION_FAILED, accountNotExistsException.getMessage(),
                    null);
        }catch(final DuplicateTransactionException duplicateTransactionException){
            throw new ResponseStatusException(HttpStatus.CONFLICT, duplicateTransactionException.getMessage(), null);
        }catch(final TransactionNotAllowedException transactionNotAllowedException){
            throw new ResponseStatusException(HttpStatus.FORBIDDEN, transactionNotAllowedException.getMessage(), null);

        }

    }

}
