package com.orangebank.codechallenge.controller.transaction.search;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.convert.converter.Converter;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.server.ResponseStatusException;

import com.orangebank.codechallenge.model.SortEnum;
import com.orangebank.codechallenge.model.persistence.Transaction;
import com.orangebank.codechallenge.service.SearchTransactionService;
import com.orangebank.codechallenge.web.model.TransactionWebResponse;

@RestController
public class SearchTransactionController{

    @Autowired
    SearchTransactionService seachTransactionService;

    @Autowired
    Converter<Transaction, TransactionWebResponse> transactionIntoTransactionWebResponseConverter;

    @GetMapping("/transactions")
    public @ResponseBody List<TransactionWebResponse> searchTransactions(
            @RequestParam(name = "account_iban", required = true)
            final String accountIban, @RequestParam(name = "sort", required = false)
            final SortEnum sort){
        final List<TransactionWebResponse> transactions = new ArrayList<>();
        seachTransactionService.searchTransactions(accountIban, sort).forEach(
                transaction -> transactions.add(transactionIntoTransactionWebResponseConverter.convert(transaction)));
        if(transactions.isEmpty()){
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, "No transactions found", null);
        }
        return transactions;
    }

}
