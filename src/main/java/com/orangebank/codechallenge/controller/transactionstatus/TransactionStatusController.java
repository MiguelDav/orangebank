package com.orangebank.codechallenge.controller.transactionstatus;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.convert.converter.Converter;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.server.ResponseStatusException;

import com.orangebank.codechallenge.exceptions.UnsupportedChannelTransactionStatusException;
import com.orangebank.codechallenge.model.TransactionStatus;
import com.orangebank.codechallenge.service.TransactionStatusService;
import com.orangebank.codechallenge.web.model.TransactionStatusWebRequest;
import com.orangebank.codechallenge.web.model.TransactionStatusWebResponse;

@RestController
public class TransactionStatusController{

    @Autowired
    private TransactionStatusService transactionStatusService;

    @Autowired
    private Converter<TransactionStatus, TransactionStatusWebResponse> transactionIntoTransactionWebResponseConverter;

    @PostMapping("/transaction-status")
    public TransactionStatusWebResponse getTransactionStatus(@RequestBody
            final TransactionStatusWebRequest getTransactionStatusWebRequest){
        try {
			return transactionIntoTransactionWebResponseConverter.convert(transactionStatusService.getTransactionStatus(
			        getTransactionStatusWebRequest.getReference(), getTransactionStatusWebRequest.getChannel()));
		} catch (UnsupportedChannelTransactionStatusException e) {
			 throw new ResponseStatusException(HttpStatus.BAD_REQUEST, e.getMessage(),
	                    null);
		}
    }

}
