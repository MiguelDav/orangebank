package com.orangebank.codechallenge.converters;

import java.math.BigDecimal;
import java.util.UUID;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.convert.converter.Converter;
import org.springframework.stereotype.Component;
import org.springframework.util.StringUtils;

import com.orangebank.codechallenge.model.persistence.Account;
import com.orangebank.codechallenge.model.persistence.Transaction;
import com.orangebank.codechallenge.utils.DateTimeFormatUtils;
import com.orangebank.codechallenge.web.model.CreateTransactionWebRequest;

@Component
public class CreateTransactionWebRequestIntoTransactionConverter
implements Converter<CreateTransactionWebRequest, Transaction>{

    @Autowired
    private DateTimeFormatUtils dateTimeFormatUtils;

    @Override
    public Transaction convert(final CreateTransactionWebRequest createTransactionWebRequest){
        final Transaction transaction = new Transaction();
        transaction.setDate(dateTimeFormatUtils.getNowAsLocalDateTime());
        transaction.setFee(BigDecimal.ZERO);
        if(StringUtils.isEmpty(createTransactionWebRequest.getReference())){
            transaction.setReference(UUID.randomUUID().toString());
        }else{
            transaction.setReference(createTransactionWebRequest.getReference());
        }
        if(createTransactionWebRequest.getDate() != null){
            transaction.setDate(dateTimeFormatUtils.parseToLocalDateTime(createTransactionWebRequest.getDate()));
        }
        transaction.setDescription(createTransactionWebRequest.getDescription());
        if(createTransactionWebRequest.getFee() != null){
            final BigDecimal fee = BigDecimal.valueOf(createTransactionWebRequest.getFee());
            transaction.setFee(fee);
        }
        final BigDecimal amount = BigDecimal.valueOf(createTransactionWebRequest.getAmount());
        transaction.setAmount(amount);
        transaction.setAccount(new Account(createTransactionWebRequest.getAccountIban()));
        return transaction;

    }

}
