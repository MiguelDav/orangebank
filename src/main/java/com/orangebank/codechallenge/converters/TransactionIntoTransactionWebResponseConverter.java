package com.orangebank.codechallenge.converters;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.convert.converter.Converter;
import org.springframework.stereotype.Component;

import com.orangebank.codechallenge.model.persistence.Transaction;
import com.orangebank.codechallenge.utils.DateTimeFormatUtils;
import com.orangebank.codechallenge.web.model.TransactionWebResponse;

@Component
public class TransactionIntoTransactionWebResponseConverter implements Converter<Transaction, TransactionWebResponse>{

    @Autowired
    DateTimeFormatUtils dateTimeFormatUtils;

    @Override
    public TransactionWebResponse convert(final Transaction transaction){
        final TransactionWebResponse transactionWebResponse = new TransactionWebResponse();
        transactionWebResponse.setAccountIban(transaction.getAccount().getAccountIban());
        transactionWebResponse.setDate(dateTimeFormatUtils.formatFromLocalDateTime(transaction.getDate()));
        transactionWebResponse.setDescription(transaction.getDescription());
        if(transaction.getFee() != null){
            transactionWebResponse.setFee(transaction.getFee().doubleValue());
        }
        transactionWebResponse.setAmount(transaction.getAmount().doubleValue());
        transactionWebResponse.setReference(transaction.getReference());
        return transactionWebResponse;

    }

}
