package com.orangebank.codechallenge.converters;

import org.springframework.core.convert.converter.Converter;
import org.springframework.stereotype.Component;

import com.orangebank.codechallenge.model.TransactionStatus;
import com.orangebank.codechallenge.web.model.TransactionStatusWebResponse;

@Component
public class TransactionStatusIntoTransactionStatusWebResponseConverter
implements Converter<TransactionStatus, TransactionStatusWebResponse>{

    @Override
    public TransactionStatusWebResponse convert(final TransactionStatus source){
        final TransactionStatusWebResponse transactionStatusWebResponse = new TransactionStatusWebResponse();
        transactionStatusWebResponse.setStatus(source.getStatus().name());
        if(source.getFee() != null){
            transactionStatusWebResponse.setFee(source.getFee().doubleValue());
        }
        if(source.getAmount() != null){
            transactionStatusWebResponse.setAmount(source.getAmount().doubleValue());
        }
        transactionStatusWebResponse.setReference(source.getReference());
        return transactionStatusWebResponse;
    }

}
