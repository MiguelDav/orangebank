package com.orangebank.codechallenge.exceptions;

@SuppressWarnings("serial")
public class AccountNotExistsException extends TransactionException{

    public AccountNotExistsException(final String msg){
        super(msg);
    }
}
