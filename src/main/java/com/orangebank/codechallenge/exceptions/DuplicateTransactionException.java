package com.orangebank.codechallenge.exceptions;

@SuppressWarnings("serial")
public class DuplicateTransactionException extends TransactionException{

    public DuplicateTransactionException(final String msg){
        super(msg);
    }
}
