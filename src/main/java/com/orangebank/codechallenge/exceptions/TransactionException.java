package com.orangebank.codechallenge.exceptions;

@SuppressWarnings("serial")
public class TransactionException extends Exception{

    public TransactionException(final String msg){
        super(msg);
    }

}
