package com.orangebank.codechallenge.exceptions;

@SuppressWarnings("serial")
public class TransactionNotAllowedException extends TransactionException{

    public TransactionNotAllowedException(final String msg){
        super(msg);
    }
}
