package com.orangebank.codechallenge.exceptions;

@SuppressWarnings("serial")
public class UnsupportedChannelTransactionStatusException extends TransactionException{

    public UnsupportedChannelTransactionStatusException(final String msg){
        super(msg);
    }
}
