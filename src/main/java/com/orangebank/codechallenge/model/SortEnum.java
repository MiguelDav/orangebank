package com.orangebank.codechallenge.model;

import org.springframework.data.domain.Sort.Direction;

public enum SortEnum{
    ASC, DESC;
    //convert to JPA
    public static Direction toJPADirection(final SortEnum se){
        if(se == null || se.equals(SortEnum.ASC)){
            return Direction.ASC;
        }
        return Direction.DESC;
    }

}
