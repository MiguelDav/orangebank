package com.orangebank.codechallenge.model;

import java.math.BigDecimal;

public class TransactionStatus{

    private String reference;
    private BigDecimal amount;
    private BigDecimal fee;
    private TransactionStatusEnum status;

    public TransactionStatus(TransactionStatusBuilder transactionStatusBuilder) {
    	this.reference = transactionStatusBuilder.getReference();
		this.amount = transactionStatusBuilder.getAmount();
		this.fee = transactionStatusBuilder.getFee();
		this.status = transactionStatusBuilder.getStatus();
	}
    

	public TransactionStatus() {
		super();
	}



	public String getReference(){
        return reference;
    }

    public void setReference(final String reference){
        this.reference = reference;
    }

    public BigDecimal getAmount(){
        return amount;
    }

    public void setAmount(final BigDecimal amount){
        this.amount = amount;
    }

    public BigDecimal getFee(){
        return fee;
    }

    public void setFee(final BigDecimal fee){
        this.fee = fee;
    }

    public TransactionStatusEnum getStatus(){
        return status;
    }

    public void setStatus(final TransactionStatusEnum status){
        this.status = status;
    }
    
    public TransactionStatusBuilder builder() {
    	return new TransactionStatusBuilder(this);
    }
    
   
    
}