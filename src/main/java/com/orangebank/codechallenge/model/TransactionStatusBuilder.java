package com.orangebank.codechallenge.model;

import java.math.BigDecimal;

import com.orangebank.codechallenge.calculation.transactionstatus.TransactionChannelPrototype;
import com.orangebank.codechallenge.model.persistence.Transaction;

public class TransactionStatusBuilder {

	private String reference;
	private BigDecimal amount;
	private BigDecimal fee;
	private TransactionStatusEnum status;
	
	

	public String getReference() {
		return reference;
	}

	public BigDecimal getAmount() {
		return amount;
	}

	public BigDecimal getFee() {
		return fee;
	}

	public TransactionStatusEnum getStatus() {
		return status;
	}

	public TransactionStatusBuilder(TransactionStatus transactionStatus) {
		this.reference = transactionStatus.getReference();
		this.amount = transactionStatus.getAmount();
		this.fee = transactionStatus.getFee();
		this.status = transactionStatus.getStatus();
	}

	public TransactionStatusBuilder() {
		super();
	}

	public TransactionStatusBuilder withReference(String reference) {
		this.reference = reference;
		return this;
	}

	public TransactionStatus build() {
		return new TransactionStatus(this);
	}

	public TransactionStatusBuilder withChannelAndTransaction(TransactionChannelPrototype transacStatusChannel,
			Transaction transaction) {
		if (transaction == null) {
			status = TransactionStatusEnum.INVALID;
			return this;
		}
		this.reference = transaction.getReference();
		this.status = transacStatusChannel.getStatusWithDateTime(transaction.getDate());
		this.amount = transaction.getAmount().subtract(transaction.getFee());
		if (!transacStatusChannel.subtractFeesInAmount()) {
			this.amount = transaction.getAmount();
			this.fee = transaction.getFee();
		}
		return this;
	}

}