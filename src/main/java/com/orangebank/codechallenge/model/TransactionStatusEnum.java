package com.orangebank.codechallenge.model;

public enum TransactionStatusEnum{
    PENDING, SETTLED, FUTURE, INVALID
}
