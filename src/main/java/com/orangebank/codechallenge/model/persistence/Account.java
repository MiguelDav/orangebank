package com.orangebank.codechallenge.model.persistence;

import java.math.BigDecimal;

import javax.persistence.Entity;
import javax.persistence.Id;

@Entity
public class Account{
    @Id
    private String accountIban;
    private BigDecimal balance;

    public Account(){
        super();
    }

    public Account(final String accountIban){
        super();
        this.accountIban = accountIban;
        this.balance = BigDecimal.ZERO;
    }

    public BigDecimal getBalance(){
        return balance;
    }

    public void setAccountIban(final String accountIban){
        this.accountIban = accountIban;
    }

    public void setBalance(final BigDecimal balance){
        this.balance = balance;
    }

    public String getAccountIban(){
        return accountIban;
    }

}