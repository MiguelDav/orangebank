package com.orangebank.codechallenge.model.persistence;

import java.math.BigDecimal;
import java.time.LocalDateTime;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.ManyToOne;

@Entity
public class Transaction{

    @Id
    private String reference;
    private LocalDateTime date;
    private BigDecimal amount;
    private BigDecimal fee;
    private String description;

    @ManyToOne(cascade = CascadeType.ALL)
    private Account account;

    public String getReference(){
        return reference;
    }

    public void setReference(final String reference){
        this.reference = reference;
    }

    public LocalDateTime getDate(){
        return date;
    }

    public void setDate(final LocalDateTime date){
        this.date = date;
    }

    public BigDecimal getAmount(){
        return amount;
    }

    public void setAmount(final BigDecimal amount){
        this.amount = amount;
    }

    public BigDecimal getFee(){
        return fee;
    }

    public void setFee(final BigDecimal fee){
        this.fee = fee;
    }

    public String getDescription(){
        return description;
    }

    public void setDescription(final String description){
        this.description = description;
    }

    public Account getAccount(){
        return account;
    }

    public void setAccount(final Account account){
        this.account = account;
    }

}