package com.orangebank.codechallenge.repository;

import org.springframework.data.repository.CrudRepository;

import com.orangebank.codechallenge.model.persistence.Account;

public interface AccountRepository extends CrudRepository<Account, String>{

}