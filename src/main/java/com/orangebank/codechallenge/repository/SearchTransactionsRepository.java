package com.orangebank.codechallenge.repository;

import java.util.List;

import org.springframework.data.domain.Sort;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.Repository;
import org.springframework.data.repository.query.Param;

import com.orangebank.codechallenge.model.persistence.Transaction;

public interface SearchTransactionsRepository extends Repository<Transaction, String>{

    @Query("SELECT t FROM Transaction t inner join Account a on a.accountIban = t.account WHERE a.accountIban=:accountIban")
    public List<Transaction> searchTransactions(@Param("accountIban") String accountIban, Sort sort);

}