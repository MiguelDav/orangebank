package com.orangebank.codechallenge.repository;

import org.springframework.data.repository.CrudRepository;

import com.orangebank.codechallenge.model.persistence.Transaction;

public interface TransactionRepository extends CrudRepository<Transaction, String>{

}