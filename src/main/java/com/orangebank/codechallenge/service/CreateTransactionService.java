package com.orangebank.codechallenge.service;

import com.orangebank.codechallenge.exceptions.AccountNotExistsException;
import com.orangebank.codechallenge.exceptions.DuplicateTransactionException;
import com.orangebank.codechallenge.exceptions.TransactionNotAllowedException;
import com.orangebank.codechallenge.model.persistence.Transaction;


public interface CreateTransactionService{

    public Transaction createTransaction(final Transaction transaction)
            throws DuplicateTransactionException, TransactionNotAllowedException, AccountNotExistsException;
}