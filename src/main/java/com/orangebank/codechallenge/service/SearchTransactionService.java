package com.orangebank.codechallenge.service;

import java.util.List;

import com.orangebank.codechallenge.model.SortEnum;
import com.orangebank.codechallenge.model.persistence.Transaction;


public interface SearchTransactionService{

    public List<Transaction> searchTransactions(final String accountIban, final SortEnum se);

}