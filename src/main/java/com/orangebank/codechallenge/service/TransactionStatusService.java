package com.orangebank.codechallenge.service;

import com.orangebank.codechallenge.exceptions.UnsupportedChannelTransactionStatusException;
import com.orangebank.codechallenge.model.ChannelEnum;
import com.orangebank.codechallenge.model.TransactionStatus;

public interface TransactionStatusService{

    public TransactionStatus getTransactionStatus(final String ref, final ChannelEnum channel) throws UnsupportedChannelTransactionStatusException;
}