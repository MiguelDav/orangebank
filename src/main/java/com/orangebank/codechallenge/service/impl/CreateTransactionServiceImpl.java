package com.orangebank.codechallenge.service.impl;

import java.math.BigDecimal;
import java.text.MessageFormat;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.orangebank.codechallenge.exceptions.AccountNotExistsException;
import com.orangebank.codechallenge.exceptions.DuplicateTransactionException;
import com.orangebank.codechallenge.exceptions.TransactionNotAllowedException;
import com.orangebank.codechallenge.model.persistence.Account;
import com.orangebank.codechallenge.model.persistence.Transaction;
import com.orangebank.codechallenge.repository.AccountRepository;
import com.orangebank.codechallenge.repository.TransactionRepository;
import com.orangebank.codechallenge.service.CreateTransactionService;

@Service
public class CreateTransactionServiceImpl implements CreateTransactionService{

    @Autowired
    TransactionRepository transactionRepository;

    @Autowired
    AccountRepository accountRepository;


    @Override
    public Transaction createTransaction(final Transaction transaction)
            throws DuplicateTransactionException, TransactionNotAllowedException, AccountNotExistsException{
        if(transactionRepository.existsById(transaction.getReference())){
            throw new DuplicateTransactionException(
                    MessageFormat.format("Transaction with ref {0} exists", transaction.getReference()));
        }
        final Optional<Account> accountExistence = accountRepository
                .findById(transaction.getAccount().getAccountIban());

        if(!accountExistence.isPresent()){
            throw new AccountNotExistsException(
                    MessageFormat.format("Account  {0} does not exists", transaction.getAccount().getAccountIban()));
        }
        final Account account = accountExistence.get();
        final BigDecimal fee = transaction.getFee() != null ? transaction.getFee() : BigDecimal.ZERO;
        final BigDecimal balance = account.getBalance().add(transaction.getAmount()).subtract(fee);
        if(balance.doubleValue() < 0){
            throw new TransactionNotAllowedException("Account balance bellow 0 is not allowed.");
        }
        account.setBalance(balance);
        accountRepository.save(account);
        transactionRepository.save(transaction);
        return transaction;

    }

}