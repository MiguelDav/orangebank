package com.orangebank.codechallenge.service.impl;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;

import com.orangebank.codechallenge.model.SortEnum;
import com.orangebank.codechallenge.model.persistence.Transaction;
import com.orangebank.codechallenge.repository.SearchTransactionsRepository;
import com.orangebank.codechallenge.service.SearchTransactionService;

@Service
public class SearchTransactionServiceImpl implements SearchTransactionService{

    protected static final String AMOUNT_COLUMN = "amount";

    @Autowired
    SearchTransactionsRepository searchTransactionsRepository;


    @Override
    public List<Transaction> searchTransactions(final String accountIban, final SortEnum se){
        final List<Transaction> transactions = new ArrayList<>();
        Sort sort = null;
        if(se != null){
            sort = new Sort(SortEnum.toJPADirection(se), Arrays.asList(AMOUNT_COLUMN));
        }
        searchTransactionsRepository.searchTransactions(accountIban, sort)
        .forEach(transactions::add);
        return transactions;
    }

}