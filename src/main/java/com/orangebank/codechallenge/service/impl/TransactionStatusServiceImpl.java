package com.orangebank.codechallenge.service.impl;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.orangebank.codechallenge.calculation.transactionstatus.TransactionChannelPrototype;
import com.orangebank.codechallenge.exceptions.UnsupportedChannelTransactionStatusException;
import com.orangebank.codechallenge.model.ChannelEnum;
import com.orangebank.codechallenge.model.TransactionStatus;
import com.orangebank.codechallenge.model.TransactionStatusBuilder;
import com.orangebank.codechallenge.model.persistence.Transaction;
import com.orangebank.codechallenge.repository.TransactionRepository;
import com.orangebank.codechallenge.service.TransactionStatusService;

@Service
public class TransactionStatusServiceImpl implements TransactionStatusService {

	@Autowired
	TransactionRepository transactionRepository;

	@Autowired
	List<TransactionChannelPrototype> transactionChannels;

	@Override
	public TransactionStatus getTransactionStatus(final String ref, final ChannelEnum channel)
			throws UnsupportedChannelTransactionStatusException {		
		final TransactionChannelPrototype transacStatusChannel = transactionChannels
				.stream()
				.filter(transactionStatusChannel -> transactionStatusChannel.getChannel() == channel)
				.findFirst()
				.orElseThrow(() -> new UnsupportedChannelTransactionStatusException("Channel " + channel.name() + " not supported"));				
		return new TransactionStatusBuilder().withChannelAndTransaction(
					transacStatusChannel,
					transactionRepository.findById(ref).orElse(null)
				).build();
	}

}