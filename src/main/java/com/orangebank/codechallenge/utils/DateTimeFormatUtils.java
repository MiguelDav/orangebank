package com.orangebank.codechallenge.utils;

import java.time.Clock;
import java.time.Instant;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.time.format.DateTimeFormatter;

import org.springframework.stereotype.Component;
import org.springframework.util.StringUtils;

/**
 * Used for Obtaining system dates
 *
 */
@Component
public class DateTimeFormatUtils{

    public LocalDateTime getNowAsLocalDateTime(){
        return LocalDateTime.now();
    }

    public LocalDateTime parseToLocalDateTime(final String date){
        if(StringUtils.isEmpty(date)){
            return null;
        }
        final Clock clock = Clock.fixed(Instant.parse(date), ZoneId.systemDefault());
        return LocalDateTime.now(clock);
    }

    public String formatFromLocalDateTime(final LocalDateTime localDateTime){
        if(localDateTime == null){
            return null;
        }
        return DateTimeFormatter.ISO_INSTANT.format(localDateTime.atZone(ZoneId.systemDefault()).toInstant());
    }

}
