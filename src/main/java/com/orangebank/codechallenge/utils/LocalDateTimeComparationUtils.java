package com.orangebank.codechallenge.utils;

import java.time.LocalDateTime;
import java.time.ZoneId;

import org.springframework.stereotype.Component;

/**
 * 
 * Utility class to compare times
 *
 */
@Component
public class LocalDateTimeComparationUtils{


    public boolean isBeforeToday(final LocalDateTime localDateTime){
        return localDateTime.toLocalDate().isBefore(LocalDateTime.now().toLocalDate());
    }

    public boolean isEqualsToToday(final LocalDateTime localDateTime){
        return localDateTime.atZone(ZoneId.systemDefault()).toLocalDate().isEqual(LocalDateTime.now().toLocalDate());
    }
}
