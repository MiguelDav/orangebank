package com.orangebank.codechallenge.web.model;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;
import com.orangebank.codechallenge.model.ChannelEnum;

@JsonPropertyOrder({"reference", "channel"})
public class TransactionStatusWebRequest{

    @JsonProperty("reference")
    private String reference;
    @JsonProperty("channel")
    private ChannelEnum channel;


    public TransactionStatusWebRequest(final String reference, final ChannelEnum channel){
        super();
        this.reference = reference;
        this.channel = channel;
    }

    public TransactionStatusWebRequest(){
        super();
    }

    @JsonProperty("reference")
    public String getReference(){
        return reference;
    }

    @JsonProperty("reference")
    public void setReference(final String reference){
        this.reference = reference;
    }

    @JsonProperty("channel")
    public ChannelEnum getChannel(){
        return channel;
    }

    @JsonProperty("channel")
    public void setChannel(final ChannelEnum channel){
        this.channel = channel;
    }

}