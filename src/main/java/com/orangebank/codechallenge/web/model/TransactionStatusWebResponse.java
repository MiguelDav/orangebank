package com.orangebank.codechallenge.web.model;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;


@JsonPropertyOrder({"reference", "status", "amount", "fee"})
public class TransactionStatusWebResponse{

    @JsonProperty("reference")
    private String reference;
    @JsonProperty("status")
    private String status;
    @JsonProperty("amount")
    @JsonInclude(Include.NON_NULL)
    private Double amount;
    @JsonProperty("fee")
    @JsonInclude(Include.NON_NULL)
    private Double fee;

    @JsonProperty("reference")
    public String getReference(){
        return reference;
    }

    @JsonProperty("reference")
    public void setReference(final String reference){
        this.reference = reference;
    }

    @JsonProperty("status")
    public String getStatus(){
        return status;
    }

    @JsonProperty("status")
    public void setStatus(final String status){
        this.status = status;
    }

    @JsonProperty("amount")
    public Double getAmount(){
        return amount;
    }

    @JsonProperty("amount")
    public void setAmount(final Double amount){
        this.amount = amount;
    }

    @JsonProperty("fee")
    public Double getFee(){
        return fee;
    }

    @JsonProperty("fee")
    public void setFee(final Double fee){
        this.fee = fee;
    }

}