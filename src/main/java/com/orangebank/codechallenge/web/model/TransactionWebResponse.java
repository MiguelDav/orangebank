package com.orangebank.codechallenge.web.model;

import java.util.Objects;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

@JsonPropertyOrder({"reference", "account_iban", "date", "amount", "fee", "description"})
public class TransactionWebResponse{

    @JsonProperty("reference")
    private String reference;
    @JsonProperty("account_iban")
    @NotBlank
    private String accountIban;
    @JsonProperty("date")
    private String date;
    @JsonProperty("amount")
    @NotNull
    private Double amount;
    @JsonProperty("fee")
    private Double fee;
    @JsonProperty("description")
    private String description;

    @JsonProperty("reference")
    public String getReference(){
        return reference;
    }

    @JsonProperty("reference")
    public void setReference(final String reference){
        this.reference = reference;
    }

    @JsonProperty("account_iban")
    public String getAccountIban(){
        return accountIban;
    }

    @JsonProperty("account_iban")
    public void setAccountIban(final String accountIban){
        this.accountIban = accountIban;
    }

    @JsonProperty("date")
    public String getDate(){
        return date;
    }

    @JsonProperty("date")
    public void setDate(final String date){
        this.date = date;
    }

    @JsonProperty("amount")
    public Double getAmount(){
        return amount;
    }

    @JsonProperty("amount")
    public void setAmount(final Double amount){
        this.amount = amount;
    }

    @JsonProperty("fee")
    public Double getFee(){
        return fee;
    }

    @JsonProperty("fee")
    public void setFee(final Double fee){
        this.fee = fee;
    }

    @JsonProperty("description")
    public String getDescription(){
        return description;
    }

    @JsonProperty("description")
    public void setDescription(final String description){
        this.description = description;
    }

    @Override
    public int hashCode(){
        return Objects.hash(accountIban, amount, date, description, fee, reference);
    }

    @Override
    public boolean equals(final Object obj){
        if(this == obj)
            return true;
        if(obj == null)
            return false;
        if(getClass() != obj.getClass())
            return false;
        final TransactionWebResponse other = (TransactionWebResponse) obj;
        return Objects.equals(accountIban, other.accountIban) && Objects.equals(amount, other.amount)
                && Objects.equals(date, other.date) && Objects.equals(description, other.description)
                && Objects.equals(fee, other.fee) && Objects.equals(reference, other.reference);
    }

}