package com.orangebank.codechallenge.calculation.transactionstatus;

import static org.hamcrest.Matchers.is;
import static org.junit.Assert.assertThat;
import static org.mockito.Mockito.when;

import java.time.LocalDateTime;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;

import com.orangebank.codechallenge.model.TransactionStatusEnum;
import com.orangebank.codechallenge.utils.LocalDateTimeComparationUtils;

@RunWith(MockitoJUnitRunner.class)
public class AtmTransactionChannelTest extends TransactionChannelPrototypeTest{


    @Mock
    private LocalDateTimeComparationUtils dateTimeUtils;
    @InjectMocks
    private AtmTransactionChannel atmTransactionChannel;

    @Before
    public void setUp(){
        configureWithDateTimeUtilsAndTransactionChannelPrototype(dateTimeUtils, atmTransactionChannel);
    }

    @Test
    public void subtractFeesInAmountMustReturnTrue(){
        assertThat(atmTransactionChannel.subtractFeesInAmount(), is(true));
    }

    @Test
    public void getTransactionStatusGreaterThanTodayMustReturnPENDINGstatus(){
        //given
        final LocalDateTime localDateTime = LocalDateTime.now();
        when(dateTimeUtils.isBeforeToday(localDateTime)).thenReturn(false);
        when(dateTimeUtils.isEqualsToToday(localDateTime)).thenReturn(false);
        final TransactionStatusEnum transactionStatusEnumExpected = TransactionStatusEnum.PENDING;
        checkGetStatusMethodWithExpectedStatus(localDateTime, transactionStatusEnumExpected);
    }




}
