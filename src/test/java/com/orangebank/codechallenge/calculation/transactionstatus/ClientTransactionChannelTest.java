package com.orangebank.codechallenge.calculation.transactionstatus;

import static org.hamcrest.Matchers.is;
import static org.junit.Assert.assertThat;
import static org.mockito.Mockito.when;

import java.time.LocalDateTime;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;

import com.orangebank.codechallenge.model.TransactionStatusEnum;
import com.orangebank.codechallenge.utils.LocalDateTimeComparationUtils;

@RunWith(MockitoJUnitRunner.class)
public class ClientTransactionChannelTest extends TransactionChannelPrototypeTest{

    @Mock
    private LocalDateTimeComparationUtils dateTimeUtils;
    @InjectMocks
    private ClientTransactionChannel clientTransactionChannel;

    @Before
    public void setUp(){
        configureWithDateTimeUtilsAndTransactionChannelPrototype(dateTimeUtils, clientTransactionChannel);
    }

    @Test
    public void subtractFeesInAmountMustReturnTrue(){
        assertThat(clientTransactionChannel.subtractFeesInAmount(), is(true));
    }

    @Test
    public void getTransactionStatusGreaterThanTodayMustReturnFUTUREstatus(){
        //given
        final LocalDateTime localDateTime = LocalDateTime.now();
        when(dateTimeUtils.isBeforeToday(localDateTime)).thenReturn(false);
        when(dateTimeUtils.isEqualsToToday(localDateTime)).thenReturn(false);
        final TransactionStatusEnum transactionStatusEnumExpected = TransactionStatusEnum.FUTURE;

        checkGetStatusMethodWithExpectedStatus(localDateTime, transactionStatusEnumExpected);
    }

}
