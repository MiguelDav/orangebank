package com.orangebank.codechallenge.calculation.transactionstatus;

import static org.hamcrest.Matchers.is;
import static org.junit.Assert.assertThat;
import static org.mockito.Mockito.when;

import java.time.LocalDateTime;

import org.junit.Test;

import com.orangebank.codechallenge.model.TransactionStatusEnum;
import com.orangebank.codechallenge.utils.LocalDateTimeComparationUtils;

public abstract class TransactionChannelPrototypeTest{

    private LocalDateTimeComparationUtils mockDateTimeUtils;

    private TransactionChannelPrototype transactionChannelPrototype;

    public void configureWithDateTimeUtilsAndTransactionChannelPrototype(final LocalDateTimeComparationUtils mockDateTimeUtils,
            final TransactionChannelPrototype transactionChannelPrototype){
        this.mockDateTimeUtils = mockDateTimeUtils;
        this.transactionChannelPrototype = transactionChannelPrototype;
    }


    @Test
    public void getTransactionStatusBeforeTodayMustReturnSETTLEDstatus(){
        //given
        final LocalDateTime localDateTime = LocalDateTime.now();
        when(mockDateTimeUtils.isBeforeToday(localDateTime)).thenReturn(true);
        final TransactionStatusEnum transactionStatusEnumExpected = TransactionStatusEnum.SETTLED;

        checkGetStatusMethodWithExpectedStatus(localDateTime, transactionStatusEnumExpected);
    }

    @Test
    public void getTransactionStatusEqualsToTodayMustReturnPENDINGstatus(){
        //given
        final LocalDateTime localDateTime = LocalDateTime.now();
        when(mockDateTimeUtils.isBeforeToday(localDateTime)).thenReturn(false);
        when(mockDateTimeUtils.isEqualsToToday(localDateTime)).thenReturn(true);
        final TransactionStatusEnum transactionStatusEnumExpected = TransactionStatusEnum.PENDING;

        checkGetStatusMethodWithExpectedStatus(localDateTime, transactionStatusEnumExpected);
    }

    protected void checkGetStatusMethodWithExpectedStatus(final LocalDateTime localDateTime,
            final TransactionStatusEnum transactionStatusEnumExpected){
        //when
        final TransactionStatusEnum transactionStatusEnumObtained = transactionChannelPrototype
                .getStatusWithDateTime(localDateTime);
        //then        
        assertThat(transactionStatusEnumObtained, is(transactionStatusEnumExpected));
    }

}
