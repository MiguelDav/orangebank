package com.orangebank.codechallenge.controller;

import java.util.List;

import org.junit.Ignore;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.web.server.LocalServerPort;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.http.HttpMethod;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.web.client.HttpClientErrorException;
import org.springframework.web.client.RestTemplate;

import com.orangebank.codechallenge.model.SortEnum;
import com.orangebank.codechallenge.model.persistence.Account;
import com.orangebank.codechallenge.repository.AccountRepository;
import com.orangebank.codechallenge.web.model.CreateTransactionWebRequest;
import com.orangebank.codechallenge.web.model.TransactionStatusWebRequest;
import com.orangebank.codechallenge.web.model.TransactionStatusWebResponse;
import com.orangebank.codechallenge.web.model.TransactionWebResponse;

@RunWith(SpringRunner.class)
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
@Ignore
public class SpringCucumberIntegrationTests  {

	private final String SERVER_URL = "http://localhost";
	private final String TRANSACTION_STATUS_ENDPOINT = "/orangebank/transaction-status";
	private final String TRANSACTIONS_ENDPOINT = "/orangebank/transactions";
	public final static String DEFAULT_ACCOUNT = "XXXXXXX_ACCOUNT";	

	private RestTemplate restTemplate;
	
	@Autowired
    private  AccountRepository accountRepository;
	
	@LocalServerPort
	protected int port;
	
	

	public SpringCucumberIntegrationTests() {

		this.restTemplate = new RestTemplate();
	}
		
	protected void addDefaultAccount() {		
		addAccount(SpringCucumberIntegrationTests.DEFAULT_ACCOUNT);
	}
	
	protected void addAccount(String accountIban) {
		accountRepository.save(new Account(accountIban));
	}
	
	protected AccountRepository getAccountRepository() {
		return accountRepository;
	}



	private String transactionStatusEndpoint() {
		return SERVER_URL + ":" + port + TRANSACTION_STATUS_ENDPOINT;
	}
	
	private String transactionEndpoint() {
		return SERVER_URL + ":" + port + TRANSACTIONS_ENDPOINT;
	}


	
	public TransactionStatusWebResponse checkStatus(TransactionStatusWebRequest checkStatusRequest) {
		return restTemplate.postForEntity(transactionStatusEndpoint(), checkStatusRequest, TransactionStatusWebResponse.class).getBody();
	}
	
	public ResponseEntity<TransactionWebResponse> createTransaction(CreateTransactionWebRequest createTransactionWebRequest) {
		ResponseEntity<TransactionWebResponse> response = null;
		try {
			response = restTemplate.postForEntity(transactionEndpoint(), createTransactionWebRequest, TransactionWebResponse.class);
		} catch (HttpClientErrorException e) {			
			return new ResponseEntity<TransactionWebResponse>(HttpStatus.valueOf(e.getRawStatusCode()));
		}
		return response;
	}

		
	public ResponseEntity<List<TransactionWebResponse>> searchTransactions(String accountIban,SortEnum sort) {
		ResponseEntity<List<TransactionWebResponse>> responseEntity = null;	
		try {
			String fullEndpoint = transactionEndpoint()+"?account_iban="+accountIban;
			if (sort!=null) {
				fullEndpoint = fullEndpoint +"&sort="+sort.name();
			}
			responseEntity = restTemplate.exchange(fullEndpoint,HttpMethod.GET, null, new ParameterizedTypeReference<List<TransactionWebResponse>>() {});
		} catch (HttpClientErrorException e) {
			return new ResponseEntity<List<TransactionWebResponse>>(HttpStatus.valueOf(e.getRawStatusCode()));
		}	
		return responseEntity;
	}

	void clean() {
		restTemplate.delete(transactionStatusEndpoint());
	}

}