package com.orangebank.codechallenge.controller.transaction;

import static org.hamcrest.Matchers.hasItems;
import static org.hamcrest.Matchers.is;
import static org.junit.Assert.assertThat;
import static org.junit.Assert.fail;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

import java.util.Arrays;
import java.util.Collections;
import java.util.List;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;
import org.springframework.core.convert.converter.Converter;
import org.springframework.http.HttpStatus;
import org.springframework.web.server.ResponseStatusException;

import com.orangebank.codechallenge.controller.transaction.search.SearchTransactionController;
import com.orangebank.codechallenge.model.SortEnum;
import com.orangebank.codechallenge.model.persistence.Transaction;
import com.orangebank.codechallenge.service.SearchTransactionService;
import com.orangebank.codechallenge.web.model.CreateTransactionWebRequest;
import com.orangebank.codechallenge.web.model.TransactionWebResponse;

@RunWith(MockitoJUnitRunner.class)
public class SeachTransactionControllerTest{
    private static final String NOT_SHOULD_REACH_MESSAGE = "Not should reach at this point";

    private static final String ACCOUNT = "account";

    private static final SortEnum SORT_ENUM_USED = SortEnum.ASC;

    @Mock
    private Converter<Transaction, TransactionWebResponse> transactionIntoTransactionWebResponseConverter;

    @Mock
    private SearchTransactionService searchTransactionService;

    @Mock
    private Converter<CreateTransactionWebRequest, Transaction> transactionWebRequestIntoTransactionConverter;
    @InjectMocks
    private SearchTransactionController searchTransactionController;

    @Test
    public void searchTransactionMustThrowExceptionWithNotFoundWhenServiceReturnsNoResults() throws Exception{
        //given
        when(searchTransactionService.searchTransactions(ACCOUNT, SORT_ENUM_USED)).thenReturn(Collections.emptyList());
        //when
        try{
            searchTransactionController.searchTransactions(ACCOUNT, SORT_ENUM_USED);
            fail(NOT_SHOULD_REACH_MESSAGE);
        }catch(final ResponseStatusException e){
            //then
            assertThat(e.getStatus(), is(HttpStatus.NOT_FOUND));
        }
    }

    @Test
    public void searchTransactionMustResults() throws Exception{
        //given
        final Transaction transaction1 = mock(Transaction.class);
        final Transaction transaction2 = mock(Transaction.class);
        final TransactionWebResponse transactionWebResponse1 = mock(TransactionWebResponse.class);
        final TransactionWebResponse transactionWebResponse2 = mock(TransactionWebResponse.class);
        when(transactionIntoTransactionWebResponseConverter.convert(transaction1)).thenReturn(transactionWebResponse1);
        when(transactionIntoTransactionWebResponseConverter.convert(transaction2)).thenReturn(transactionWebResponse2);
        when(searchTransactionService.searchTransactions(ACCOUNT, SORT_ENUM_USED))
        .thenReturn(Arrays.asList(transaction1, transaction2));
        //when
        final List<TransactionWebResponse> transactionListObtained = searchTransactionController.searchTransactions(
                ACCOUNT,
                SORT_ENUM_USED);
        //then
        assertThat(transactionListObtained, hasItems(transactionWebResponse1, transactionWebResponse2));
        assertThat(transactionListObtained.size(), is(2));
    }



}
