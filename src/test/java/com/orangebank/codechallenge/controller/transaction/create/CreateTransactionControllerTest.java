package com.orangebank.codechallenge.controller.transaction.create;

import static org.hamcrest.Matchers.is;
import static org.junit.Assert.assertThat;
import static org.junit.Assert.fail;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;
import org.springframework.core.convert.converter.Converter;
import org.springframework.http.HttpStatus;
import org.springframework.web.server.ResponseStatusException;

import com.orangebank.codechallenge.controller.transaction.create.CreateTransactionController;
import com.orangebank.codechallenge.exceptions.AccountNotExistsException;
import com.orangebank.codechallenge.exceptions.DuplicateTransactionException;
import com.orangebank.codechallenge.exceptions.TransactionNotAllowedException;
import com.orangebank.codechallenge.model.persistence.Transaction;
import com.orangebank.codechallenge.service.CreateTransactionService;
import com.orangebank.codechallenge.web.model.CreateTransactionWebRequest;
import com.orangebank.codechallenge.web.model.TransactionWebResponse;

@RunWith(MockitoJUnitRunner.class)
public class CreateTransactionControllerTest{
    private static final String NOT_SHOULD_REACH_MESSAGE = "Not should reach at this point";


    @Mock
    private Converter<Transaction, TransactionWebResponse> transactionIntoTransactionWebResponseConverter;

    @Mock
    private CreateTransactionService createTransactionService;

    @Mock
    private Converter<CreateTransactionWebRequest, Transaction> transactionWebRequestIntoTransactionConverter;
    @InjectMocks
    private CreateTransactionController createTransactionController;

    @Test
    public void createTransactionMustReturnExceptionWithPreconditionFailedWhenAccountNotExistsExceptionOccurs()
            throws Exception{
        testCreateTransactionWithExceptionOccursAndHttpStatusExpected(new AccountNotExistsException(null),
                HttpStatus.PRECONDITION_FAILED);
    }

    @Test
    public void createTransactionMustReturnExceptionWithConflictWhenDuplicateTransactionExceptionOccurs()
            throws Exception{
        testCreateTransactionWithExceptionOccursAndHttpStatusExpected(new DuplicateTransactionException(null),
                HttpStatus.CONFLICT);
    }

    @Test
    public void createTransactionMustReturnExceptionWithForbiddenWhenTransactionNotAllowdExceptionOccurs()
            throws Exception{
        testCreateTransactionWithExceptionOccursAndHttpStatusExpected(new TransactionNotAllowedException(null),
                HttpStatus.FORBIDDEN);
    }

    private void testCreateTransactionWithExceptionOccursAndHttpStatusExpected(final Exception exception,
            final HttpStatus httpStatus) throws Exception{
        //given
        final CreateTransactionWebRequest transactionWebRequest = mock(CreateTransactionWebRequest.class);
        final Transaction transaction = mock(Transaction.class);
        when(transactionWebRequestIntoTransactionConverter.convert(transactionWebRequest)).thenReturn(transaction);
        when(createTransactionService.createTransaction(transaction)).thenThrow(exception);
        //when
        try{
            createTransactionController.createTransaction(transactionWebRequest);
            fail(NOT_SHOULD_REACH_MESSAGE);
        }catch(final ResponseStatusException e){
            //then
            assertThat(e.getStatus(), is(httpStatus));
        }
    }

    @Test
    public void createShoudCallToService() throws Exception{
        //given
        final CreateTransactionWebRequest transactionWebRequest = mock(CreateTransactionWebRequest.class);
        final Transaction transaction = mock(Transaction.class);
        when(transactionWebRequestIntoTransactionConverter.convert(transactionWebRequest)).thenReturn(transaction);
        //when
        createTransactionController.createTransaction(transactionWebRequest);

        //then
        verify(createTransactionService, times(1)).createTransaction(transaction);

    }

}
