package com.orangebank.codechallenge.controller.transaction.create;

import org.junit.runner.RunWith;

import cucumber.api.CucumberOptions;
import cucumber.api.junit.Cucumber;

@RunWith(Cucumber.class)
@CucumberOptions(features = "src/test/resources/features/create-transactions.feature", plugin = {"pretty",
        "html:target/create-transactions"}, glue = {"com.orangebank.codechallenge.controller.transaction.create"})
public class CreateTransactionCucumberIntegrationTest{

}