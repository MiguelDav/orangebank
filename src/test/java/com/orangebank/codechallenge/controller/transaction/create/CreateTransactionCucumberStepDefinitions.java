package com.orangebank.codechallenge.controller.transaction.create;



import static org.junit.Assert.assertThat;
import static org.junit.Assert.assertTrue;

import java.math.BigDecimal;
import java.util.HashMap;
import java.util.List;
import java.util.UUID;
import java.util.function.Predicate;

import org.hamcrest.Matchers;
import org.junit.Ignore;
import org.springframework.http.ResponseEntity;

import com.orangebank.codechallenge.controller.SpringCucumberIntegrationTests;
import com.orangebank.codechallenge.model.persistence.Account;
import com.orangebank.codechallenge.web.model.CreateTransactionWebRequest;
import com.orangebank.codechallenge.web.model.TransactionWebResponse;

import cucumber.api.java8.En;

@Ignore
public class CreateTransactionCucumberStepDefinitions extends SpringCucumberIntegrationTests implements En{
    private static final String ACCOUNT = "ACCOUNT";
    private static final String LAST_RESPONSE = "LAST_RESPONSE";

    //private final Logger log = LoggerFactory.getLogger(TransactionCucumberStepDefinitions.class);

    public CreateTransactionCucumberStepDefinitions() {

        final HashMap<String, Object> map = new HashMap<String, Object>();

        Given("An Acount that does not exist in the system", () -> {
            final String account = UUID.randomUUID().toString();			
            map.put(ACCOUNT, account);			
        });


        Given("An Account {word} that exists in the system", (final String account) -> {
            addAccount(account);
            map.put(ACCOUNT, account);			
        });




        Given("account has balance of {double}", (final Double balance) -> {
            final String accountId = (String) map.get(ACCOUNT);
            final Account account = new Account(accountId);
            account.setBalance(BigDecimal.valueOf(balance));
            getAccountRepository().save(account);
        });

        Given("A transaction {word} that exist in our system", (final String reference) -> {
            addDefaultAccount();
            final CreateTransactionWebRequest createTransactionWebRequest = new CreateTransactionWebRequest();
            createTransactionWebRequest.setReference(reference);
            createTransactionWebRequest.setAccountIban(DEFAULT_ACCOUNT);
            createTransactionWebRequest.setAmount(BigDecimal.ONE.doubleValue());		
            final ResponseEntity<TransactionWebResponse> response = createTransaction(createTransactionWebRequest);
            map.put(LAST_RESPONSE, response);
        });

        When("I add one transaction with ref {word} in the system", (final String reference) -> {
            final CreateTransactionWebRequest createTransactionWebRequest = new CreateTransactionWebRequest();
            createTransactionWebRequest.setReference(reference);
            createTransactionWebRequest.setAccountIban((String) map.get(ACCOUNT));
            createTransactionWebRequest.setAmount(BigDecimal.ONE.doubleValue());			
            final ResponseEntity<TransactionWebResponse> response = createTransaction(createTransactionWebRequest);
            map.put(LAST_RESPONSE, response);
        });


        When("I add one transaction with ref {word} for Account {word} with amount={double} and fee={double}", (final String reference, final String account, final Double amount, final Double fee) -> {
            final CreateTransactionWebRequest createTransactionWebRequest = new CreateTransactionWebRequest();
            createTransactionWebRequest.setReference(reference);
            createTransactionWebRequest.setAccountIban(account);
            createTransactionWebRequest.setAmount(amount);
            createTransactionWebRequest.setFee(fee);	
            final ResponseEntity<TransactionWebResponse> response = createTransaction(createTransactionWebRequest);
            map.put(LAST_RESPONSE, response);						
        });

        When("I add one transaction with ref {word} for Account {word} with amount={double} and fee={double} and description={string} and date={string}", (final String reference, final String account, final Double amount, final Double fee,final String description, final String date) -> {
            final CreateTransactionWebRequest createTransactionWebRequest = new CreateTransactionWebRequest();
            createTransactionWebRequest.setReference(reference);
            createTransactionWebRequest.setAccountIban(account);
            createTransactionWebRequest.setAmount(amount);
            createTransactionWebRequest.setFee(fee);
            createTransactionWebRequest.setDescription(description);
            createTransactionWebRequest.setDate(date);
            final ResponseEntity<TransactionWebResponse> response = createTransaction(createTransactionWebRequest);
            map.put(LAST_RESPONSE, response);						
        });


        Then("the system should return http status {int}", (final Integer httpStatus) -> {
            @SuppressWarnings("rawtypes")
            final
            ResponseEntity response  = (ResponseEntity) map.get(LAST_RESPONSE);
            assertThat(httpStatus, Matchers.is(response.getStatusCode().value()));
        });

        Then("the transaction {word} for Account {word} exists in the system with amount={double} and fee={double} and description={string} and date={string}", (final String reference, final String account, final Double amount, final Double fee,final String description, final String date) -> {			
            final List<TransactionWebResponse> transactions = searchTransactions(account,null).getBody();
            final TransactionWebResponse transactionWebResponseExpected = new TransactionWebResponse();
            transactionWebResponseExpected.setAccountIban(account);
            transactionWebResponseExpected.setAmount(amount);
            transactionWebResponseExpected.setDate(date);
            transactionWebResponseExpected.setDescription(description);
            transactionWebResponseExpected.setFee(fee);
            transactionWebResponseExpected.setReference(reference);

            final Predicate<TransactionWebResponse> existsExpectedTransaction = new Predicate<TransactionWebResponse>() {
                @Override
                public boolean test(final TransactionWebResponse t) {
                    return t.equals(transactionWebResponseExpected);
                }
            };
            assertTrue(transactions.stream().anyMatch(existsExpectedTransaction));							
        });



    }



}