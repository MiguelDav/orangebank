package com.orangebank.codechallenge.controller.transaction.search;



import static org.junit.Assert.assertThat;

import java.util.Comparator;
import java.util.HashMap;
import java.util.List;
import java.util.Random;
import java.util.UUID;
import java.util.stream.Collectors;

import org.hamcrest.Matchers;
import org.junit.Ignore;
import org.springframework.http.ResponseEntity;

import com.orangebank.codechallenge.controller.SpringCucumberIntegrationTests;
import com.orangebank.codechallenge.model.SortEnum;
import com.orangebank.codechallenge.web.model.CreateTransactionWebRequest;
import com.orangebank.codechallenge.web.model.TransactionWebResponse;

import cucumber.api.java8.En;

@Ignore
public class SearchTransactionCucumberStepDefinitions extends SpringCucumberIntegrationTests implements En{
    private static final String ACCOUNT = "ACCOUNT";
    private static final String LAST_RESPONSE = "LAST_RESPONSE";

    //private final Logger log = LoggerFactory.getLogger(TransactionCucumberStepDefinitions.class);

    public SearchTransactionCucumberStepDefinitions() {

        final HashMap<String, Object> map = new HashMap<String, Object>();

        Given("An Acount that does not exist in the system", () -> {
            final String account = UUID.randomUUID().toString();			
            map.put(ACCOUNT, account);			
        });


        Given("An Account {word} that exists in the system", (final String account) -> {
            addAccount(account);
            map.put(ACCOUNT, account);			
        });


        Given("{int} transactions with random values", (final Integer numOftransactions) -> {
            final String account = (String) map.get(ACCOUNT);
            final Random r = new Random(); 
            for (int i = 0; i < numOftransactions; i++) {
                final CreateTransactionWebRequest createTransactionWebRequest = new CreateTransactionWebRequest();
                createTransactionWebRequest.setReference(UUID.randomUUID().toString());
                createTransactionWebRequest.setAccountIban(account);				 
                createTransactionWebRequest.setAmount(10.0 + r.nextDouble() * 20.0);		
                createTransaction(createTransactionWebRequest);
            }			
        });


        When("I search transactions with {word} order for Amount", (final String sort) -> {
            final String accountId = (String) map.get(ACCOUNT);						
            SortEnum sortEnum = null;
            try {
                sortEnum = SortEnum.valueOf(sort);
            } catch (final Exception e) {
                //sortEnum will be null
            }
            @SuppressWarnings("rawtypes")
            final
            ResponseEntity response = searchTransactions(accountId, sortEnum);
            map.put(LAST_RESPONSE, response);
        });


        Then("the system should return http status {int}", (final Integer httpStatus) -> {
            @SuppressWarnings("rawtypes")
            final
            ResponseEntity response  = (ResponseEntity) map.get(LAST_RESPONSE);
            assertThat(httpStatus, Matchers.is(response.getStatusCode().value()));
        });

        Then("there are {int} transactions sorted by Amount in {word} order", (final Integer numOfTransactions, final String order) -> {			
            @SuppressWarnings("unchecked")
            final ResponseEntity<List<TransactionWebResponse>> response  = (ResponseEntity<List<TransactionWebResponse>>) map.get(LAST_RESPONSE);			
            assertThat(response.getBody().size(), Matchers.is(numOfTransactions));
            final Comparator<TransactionWebResponse> comparator = new Comparator<TransactionWebResponse>() {
                @Override
                public int compare(final TransactionWebResponse o1, final TransactionWebResponse o2) {
                    if (order.equalsIgnoreCase("descending")) {
                        return -1*o1.getAmount().compareTo(o2.getAmount());
                    }
                    return o1.getAmount().compareTo(o2.getAmount());
                }
            };			
            final List<TransactionWebResponse> transactionsSorted =  response.getBody().stream().sorted(comparator).collect(Collectors.toList());
            assertThat(transactionsSorted, Matchers.is(response.getBody()));


        });



    }



}