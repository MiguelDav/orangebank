package com.orangebank.codechallenge.controller.transaction.search;

import org.junit.runner.RunWith;

import cucumber.api.CucumberOptions;
import cucumber.api.junit.Cucumber;

@RunWith(Cucumber.class)
@CucumberOptions(features = "src/test/resources/features/search-transactions.feature", plugin = {"pretty",
"html:target/search-transactions"}, glue = {"com.orangebank.codechallenge.controller.transaction.search"})
public class SearchTransactionsCucumberIntegrationTest{

}