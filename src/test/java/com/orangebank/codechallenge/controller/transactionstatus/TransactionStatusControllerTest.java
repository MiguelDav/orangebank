package com.orangebank.codechallenge.controller.transactionstatus;

import static org.hamcrest.Matchers.is;
import static org.junit.Assert.assertThat;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;

import com.orangebank.codechallenge.converters.TransactionStatusIntoTransactionStatusWebResponseConverter;
import com.orangebank.codechallenge.model.ChannelEnum;
import com.orangebank.codechallenge.model.TransactionStatus;
import com.orangebank.codechallenge.service.TransactionStatusService;
import com.orangebank.codechallenge.web.model.TransactionStatusWebRequest;
import com.orangebank.codechallenge.web.model.TransactionStatusWebResponse;

@RunWith(MockitoJUnitRunner.class)
public class TransactionStatusControllerTest{
    private static final String REFERENCE = "reference";

    @Mock
    private TransactionStatusIntoTransactionStatusWebResponseConverter transactionIntoTransactionWebResponseConverter;

    @Mock
    private TransactionStatusService transactionStatusService;
    @InjectMocks
    private TransactionStatusController transactionStatusController;

    @Test
    public void testGetTransactionStatus() throws Exception{
        //given
        final ChannelEnum channelUsed = ChannelEnum.ATM;
        final TransactionStatusWebRequest transactionStatusWebRequest = new TransactionStatusWebRequest(REFERENCE,
                channelUsed);
        final TransactionStatus transactionStatus = mock(TransactionStatus.class);
        final TransactionStatusWebResponse transactionStatusWebResponse = mock(TransactionStatusWebResponse.class);
        when(transactionIntoTransactionWebResponseConverter.convert(transactionStatus))
        .thenReturn(transactionStatusWebResponse);
        when(transactionStatusService.getTransactionStatus(REFERENCE, channelUsed)).thenReturn(transactionStatus);
        //when
        final TransactionStatusWebResponse response = transactionStatusController
                .getTransactionStatus(transactionStatusWebRequest);
        //then
        assertThat(response, is(transactionStatusWebResponse));
    }

}
