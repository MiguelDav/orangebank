package com.orangebank.codechallenge.controller.transactionstatus;

import org.junit.runner.RunWith;

import cucumber.api.CucumberOptions;
import cucumber.api.junit.Cucumber;

@RunWith(Cucumber.class)
@CucumberOptions(features = "src/test/resources/features/transaction-status.feature", plugin = {"pretty", "html:target/transaction-status"}, glue = {"com.orangebank.codechallenge.controller.transactionstatus"})
public class TransactionStatusCucumberIntegrationTest {
		

}