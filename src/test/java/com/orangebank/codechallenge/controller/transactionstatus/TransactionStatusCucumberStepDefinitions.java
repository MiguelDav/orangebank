package com.orangebank.codechallenge.controller.transactionstatus;

import static org.junit.Assert.assertThat;
import static org.junit.Assert.assertTrue;

import java.time.LocalDateTime;
import java.time.ZoneId;
import java.time.format.DateTimeFormatter;
import java.util.HashMap;
import java.util.List;
import java.util.UUID;

import org.hamcrest.Matchers;
import org.junit.Ignore;
import org.springframework.http.HttpStatus;
import org.springframework.web.client.HttpClientErrorException;

import com.orangebank.codechallenge.controller.SpringCucumberIntegrationTests;
import com.orangebank.codechallenge.model.ChannelEnum;
import com.orangebank.codechallenge.web.model.CreateTransactionWebRequest;
import com.orangebank.codechallenge.web.model.TransactionStatusWebRequest;
import com.orangebank.codechallenge.web.model.TransactionStatusWebResponse;
import com.orangebank.codechallenge.web.model.TransactionWebResponse;

import cucumber.api.java8.En;

@Ignore
public class TransactionStatusCucumberStepDefinitions extends SpringCucumberIntegrationTests implements En {
	private static final String TRANSACTION_STORED = "TRANSACTION_STORED";
	private static final String REFERENCE = "REFERENCE";

	public TransactionStatusCucumberStepDefinitions() {
		
		final HashMap<String, Object> map = new HashMap<String, Object>();	

		Given("A transaction that is not stored in our system", () -> {
			final String reference = UUID.randomUUID().toString();			
			map.put(REFERENCE, reference);			
			try { 
			final List<TransactionWebResponse> transactionWebResponses = searchTransactions(DEFAULT_ACCOUNT,null).getBody();
			assertTrue(transactionWebResponses == null || !transactionWebResponses.stream().anyMatch(transaction -> transaction.getReference().equals(reference)));
			} catch (final HttpClientErrorException e) {
				assertThat(e.getStatusCode(), Matchers.is(HttpStatus.NOT_FOUND));
			}
		});
		
			   
		Given("A transaction that is stored in our system with amount={double} and fee={double} and transaction date is {string} today", (final Double amount, final Double fee, final String when) -> {
			addDefaultAccount();
			final String reference = UUID.randomUUID().toString();			
			map.put(REFERENCE, reference);
			final CreateTransactionWebRequest createTransactionWebRequest = new CreateTransactionWebRequest();
			createTransactionWebRequest.setReference(reference);
			createTransactionWebRequest.setAccountIban(DEFAULT_ACCOUNT);
			createTransactionWebRequest.setAmount(amount);		
			createTransactionWebRequest.setFee(fee);	
			LocalDateTime localDateTime = LocalDateTime.now();
			if (when.equalsIgnoreCase("BEFORE")) {
				localDateTime = localDateTime.plusDays(-2);
			} else {
				if (when.equalsIgnoreCase("GREATER THAN")) {
					localDateTime = localDateTime.plusDays(+2);
				} 
			}
			
			createTransactionWebRequest.setDate(DateTimeFormatter.ISO_INSTANT.format(localDateTime.atZone(ZoneId.systemDefault()).toInstant()));			
			final TransactionWebResponse transactionWebResponse = createTransaction(createTransactionWebRequest).getBody();
			map.put(TRANSACTION_STORED, createTransactionWebRequest);
			assertThat(reference, Matchers.is(transactionWebResponse.getReference()));					
		});
		
		

		When("I check the status from {word} channel", (final String channel) -> {
			final TransactionStatusWebRequest checkStatusRequest = new TransactionStatusWebRequest();
			if (channel.toUpperCase().equals("ANY")) {
				checkStatusRequest.setChannel(ChannelEnum.CLIENT);
			} else {
				checkStatusRequest.setChannel(ChannelEnum.valueOf(channel));
			}
			final String reference = (String) map.get(REFERENCE);
			checkStatusRequest.setReference(reference);
			final TransactionStatusWebResponse transactionStatusWebResponse = checkStatus(checkStatusRequest);
			map.put("STATUS_RESPONSE",transactionStatusWebResponse);
		});
//
		Then("The system returns the status '{word}'", (final String status) -> {
			final TransactionStatusWebResponse transactionStatusWebResponse = (TransactionStatusWebResponse) map.get("STATUS_RESPONSE");
			assertTrue(transactionStatusWebResponse.getStatus().equals(status));			
		});
		
		
		Then("The amount is {double} \\(substracting the fee)", (final Double amount) -> {
			final TransactionStatusWebResponse transactionStatusWebResponse = (TransactionStatusWebResponse) map.get("STATUS_RESPONSE");
			final CreateTransactionWebRequest transactionStored = (CreateTransactionWebRequest) map.get(TRANSACTION_STORED);
			assertTrue(amount==(transactionStored.getAmount().doubleValue()-transactionStored.getFee().doubleValue()));			
			assertTrue(transactionStatusWebResponse.getFee()==null);
		});
		
		Then("The amount is {double}", (final Double amount) -> {
			final TransactionStatusWebResponse transactionStatusWebResponse = (TransactionStatusWebResponse) map.get("STATUS_RESPONSE");			
			assertTrue(amount==transactionStatusWebResponse.getAmount().doubleValue());						
		});
		Then("The fee is {double}", (final Double fee) -> {
			final TransactionStatusWebResponse transactionStatusWebResponse = (TransactionStatusWebResponse) map.get("STATUS_RESPONSE");			
			assertTrue(fee==transactionStatusWebResponse.getFee().doubleValue());						
		});
		
		
//
//		Then("the bag should contain {int} {word}", (Integer quantity, String something) -> {
//			assertNumberOfTimes(quantity, something, false);
//		});

	}

//	private void assertNumberOfTimes(final int quantity, final String something, final boolean onlyThat) {
//		final List<String> things = new ArrayList<String>();// getContents().getThings();
//		log.info("Expecting {} times {}. The bag contains {}", quantity, something, things);
//		final int timesInList = Collections.frequency(things, something);
//		assertThat(timesInList).isEqualTo(quantity);
//		if (onlyThat) {
//			assertThat(timesInList).isEqualTo(things.size());
//		}
//	}

}