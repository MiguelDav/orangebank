package com.orangebank.codechallenge.converters;

import static org.hamcrest.Matchers.is;
import static org.hamcrest.Matchers.notNullValue;
import static org.junit.Assert.assertThat;
import static org.mockito.Mockito.when;

import java.time.Clock;
import java.time.Instant;
import java.time.LocalDateTime;
import java.time.ZoneId;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;

import com.orangebank.codechallenge.model.persistence.Transaction;
import com.orangebank.codechallenge.utils.DateTimeFormatUtils;
import com.orangebank.codechallenge.web.model.CreateTransactionWebRequest;

@RunWith(MockitoJUnitRunner.class)
public class CreateTransactionWebRequestIntoTransactionConverterTest {

    private static final double FEE_VALUE = 5d;

    private static final String DESCRIPTION = "description";

    private static final String REFERENCE = "reference";

    private static final String DATE_TIME_USED = "2014-12-22T10:15:30.00Z";

    @Mock
    DateTimeFormatUtils dateTimeFormatUtils;

    @InjectMocks
    CreateTransactionWebRequestIntoTransactionConverter converter;

    private static final double AMOUNT = 10d;
    private static final String ACCOUNT = "ACCOUNT";

    @Test
    public void testConvertWithSimpleTransaction() throws Exception{
        //given
        final CreateTransactionWebRequest createTransactionWebRequest = new CreateTransactionWebRequest();
        createTransactionWebRequest.setAccountIban(ACCOUNT);
        createTransactionWebRequest.setAmount(AMOUNT);
        final LocalDateTime dateTime = getDateTime();
        when(dateTimeFormatUtils.getNowAsLocalDateTime()).thenReturn(dateTime);
        //when
        final Transaction transactionConverted = converter.convert(createTransactionWebRequest);
        //then
        assertThat(transactionConverted.getAccount().getAccountIban(), is(ACCOUNT));
        assertThat(transactionConverted.getAccount().getBalance().doubleValue(), is(0d));
        assertThat(transactionConverted.getAmount().doubleValue(), is(AMOUNT));
        assertThat(transactionConverted.getDate(), is(dateTime));
        assertThat(transactionConverted.getFee().doubleValue(), is(0d));
        assertThat(transactionConverted.getReference(), is(notNullValue()));
    }

    @Test
    public void testConvertWithComplexTransaction() throws Exception{
        //given
        final CreateTransactionWebRequest createTransactionWebRequest = new CreateTransactionWebRequest();
        createTransactionWebRequest.setAccountIban(ACCOUNT);
        createTransactionWebRequest.setAmount(AMOUNT);
        createTransactionWebRequest.setDate(DATE_TIME_USED);
        final LocalDateTime dateTime = getDateTime();
        when(dateTimeFormatUtils.parseToLocalDateTime(DATE_TIME_USED)).thenReturn(dateTime);
        createTransactionWebRequest.setReference(REFERENCE);
        createTransactionWebRequest.setFee(FEE_VALUE);
        createTransactionWebRequest.setDescription(DESCRIPTION);
        //when
        final Transaction transactionConverted = converter.convert(createTransactionWebRequest);
        //then
        assertThat(transactionConverted.getAccount().getAccountIban(), is(ACCOUNT));
        assertThat(transactionConverted.getAccount().getBalance().doubleValue(), is(0d));
        assertThat(transactionConverted.getAmount().doubleValue(), is(AMOUNT));
        assertThat(transactionConverted.getDate(), is(dateTime));
        assertThat(transactionConverted.getFee().doubleValue(), is(FEE_VALUE));
        assertThat(transactionConverted.getReference(), is(REFERENCE));
        assertThat(transactionConverted.getDescription(), is(DESCRIPTION));

    }

    private LocalDateTime getDateTime(){
        final Clock clock = Clock.fixed(Instant.parse(DATE_TIME_USED), ZoneId.of("UTC"));
        final LocalDateTime dateTime = LocalDateTime.now(clock);
        return dateTime;
    }

}
