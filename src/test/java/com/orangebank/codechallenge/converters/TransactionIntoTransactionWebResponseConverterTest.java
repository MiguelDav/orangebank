package com.orangebank.codechallenge.converters;

import static org.hamcrest.Matchers.is;
import static org.junit.Assert.assertThat;
import static org.mockito.Mockito.when;

import java.math.BigDecimal;
import java.time.LocalDateTime;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;

import com.orangebank.codechallenge.model.persistence.Account;
import com.orangebank.codechallenge.model.persistence.Transaction;
import com.orangebank.codechallenge.utils.DateTimeFormatUtils;
import com.orangebank.codechallenge.web.model.TransactionWebResponse;

@RunWith(MockitoJUnitRunner.class)
public class TransactionIntoTransactionWebResponseConverterTest{

    private static final String REFERENCE = "reference";
    private static final String DESCRIPTION = "description";
    private static final String DATE_TIME_FORMATTED = "2014-12-22T10:15:30.00Z";
    private static final double AMOUNT = 10d;
    private static final double FEE = 1d;
    private static final String ACCOUNT = "ACCOUNT";


    @Mock
    DateTimeFormatUtils dateTimeFormatUtils;

    @InjectMocks
    TransactionIntoTransactionWebResponseConverter converter;

    @Test
    public void testConvertWithSimpleTransaction() throws Exception{
        //given
        final Transaction transaction = new Transaction();
        transaction.setAccount(new Account(ACCOUNT));
        transaction.setAmount(BigDecimal.TEN);
        when(dateTimeFormatUtils.formatFromLocalDateTime(null)).thenReturn(null);
        //when
        final TransactionWebResponse transactionConverted = converter.convert(transaction);
        //then
        assertThat(transactionConverted.getAccountIban(), is(ACCOUNT));
        assertThat(transactionConverted.getAmount().doubleValue(), is(AMOUNT));
    }

    @Test
    public void testConvertWithComplexTransaction() throws Exception{
        //given
        final Transaction transaction = new Transaction();
        transaction.setAccount(new Account(ACCOUNT));
        transaction.setAmount(BigDecimal.TEN);
        final LocalDateTime localDateTime = LocalDateTime.now();
        transaction.setDate(localDateTime);
        transaction.setDescription(DESCRIPTION);
        transaction.setFee(BigDecimal.ONE);
        transaction.setReference(REFERENCE);
        when(dateTimeFormatUtils.formatFromLocalDateTime(localDateTime)).thenReturn(DATE_TIME_FORMATTED);
        //when
        final TransactionWebResponse transactionConverted = converter.convert(transaction);
        //then
        assertThat(transactionConverted.getAccountIban(), is(ACCOUNT));
        assertThat(transactionConverted.getFee().doubleValue(), is(FEE));
        assertThat(transactionConverted.getAmount().doubleValue(), is(AMOUNT));
        assertThat(transactionConverted.getDate(), is(DATE_TIME_FORMATTED));
        assertThat(transactionConverted.getReference(), is(REFERENCE));
        assertThat(transactionConverted.getDescription(), is(DESCRIPTION));
    }


}
