package com.orangebank.codechallenge.converters;

import static org.hamcrest.Matchers.is;
import static org.junit.Assert.assertThat;

import java.math.BigDecimal;

import org.junit.Test;

import com.orangebank.codechallenge.model.TransactionStatus;
import com.orangebank.codechallenge.model.TransactionStatusEnum;
import com.orangebank.codechallenge.web.model.TransactionStatusWebResponse;

public class TransactionStatusIntoTransactionStatusWebResponseConverterTest{

    private static final String REFERENCE = "reference";
    TransactionStatusIntoTransactionStatusWebResponseConverter converter = new TransactionStatusIntoTransactionStatusWebResponseConverter();

    @Test
    public void testConvertWithSimpleTransactionStatus() throws Exception{
        //given
        final TransactionStatus transactionStatus = new TransactionStatus();
        transactionStatus.setStatus(TransactionStatusEnum.PENDING);
        //when
        final TransactionStatusWebResponse transactionStatusWebResponse = converter.convert(transactionStatus);
        //then
        assertThat(transactionStatusWebResponse.getStatus(), is(TransactionStatusEnum.PENDING.name()));
    }

    @Test
    public void testConvertWithComplexTransactionStatus() throws Exception{
        //given
        final TransactionStatus transactionStatus = new TransactionStatus();
        transactionStatus.setStatus(TransactionStatusEnum.FUTURE);
        transactionStatus.setFee(BigDecimal.ONE);
        transactionStatus.setReference(REFERENCE);
        transactionStatus.setAmount(BigDecimal.TEN);
        //when
        final TransactionStatusWebResponse transactionStatusWebResponse = converter.convert(transactionStatus);
        //then
        assertThat(transactionStatusWebResponse.getStatus(), is(TransactionStatusEnum.FUTURE.name()));
        assertThat(transactionStatusWebResponse.getFee(), is(BigDecimal.ONE.doubleValue()));
        assertThat(transactionStatusWebResponse.getAmount(), is(BigDecimal.TEN.doubleValue()));
        assertThat(transactionStatusWebResponse.getReference(), is(REFERENCE));

    }
}
