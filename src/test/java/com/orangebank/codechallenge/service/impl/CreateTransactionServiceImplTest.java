package com.orangebank.codechallenge.service.impl;

import static org.junit.Assert.assertTrue;
import static org.junit.Assert.fail;
import static org.mockito.Mockito.doReturn;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;

import java.math.BigDecimal;
import java.util.Optional;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.ArgumentMatcher;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.MockitoJUnitRunner;

import com.orangebank.codechallenge.exceptions.AccountNotExistsException;
import com.orangebank.codechallenge.exceptions.DuplicateTransactionException;
import com.orangebank.codechallenge.exceptions.TransactionNotAllowedException;
import com.orangebank.codechallenge.model.persistence.Account;
import com.orangebank.codechallenge.model.persistence.Transaction;
import com.orangebank.codechallenge.repository.AccountRepository;
import com.orangebank.codechallenge.repository.TransactionRepository;

@RunWith(MockitoJUnitRunner.class)
public class CreateTransactionServiceImplTest{
    private static final String ANY = "ANY";
    private static final String ACC_IBAN = "accIban";
    @Mock
    private AccountRepository accountRepository;
    @Mock
    private TransactionRepository transactionRepository;
    @InjectMocks
    private CreateTransactionServiceImpl transactionService;


    @Test
    public void createTransactionMustThrowExceptionWhenTransactionExists() throws Exception{
        final String refId = ANY;
        doReturn(true).when(transactionRepository).existsById(refId);
        final Transaction transaction = new Transaction();
        transaction.setReference(refId);
        try{
            transactionService.createTransaction(transaction);
            fail("Not should reach at this point");
        }catch(final Exception e){
            assertTrue(e instanceof DuplicateTransactionException);
        }
    }

    @Test
    public void createTransactionMustThrowExceptionWhenAccountDoesNotExist() throws Exception{
        final String refId = ANY;
        final String accountId = ACC_IBAN;
        doReturn(false).when(transactionRepository).existsById(refId);
        final Transaction transaction = new Transaction();
        transaction.setReference(refId);
        transaction.setAccount(new Account(accountId));
        final Optional<Account> accountExistence = Optional.empty();
        doReturn(accountExistence).when(accountRepository).findById(accountId);
        try{
            transactionService.createTransaction(transaction);
            fail("Not should reach at this point");
        }catch(final Exception e){
            assertTrue(e instanceof AccountNotExistsException);
        }
    }

    @Test
    public void createTransactionMustThrowExceptionWhenAccountBalanceIsGoingToBeBellowZero() throws Exception{
        final String refId = ANY;
        final String accountId = ACC_IBAN;
        doReturn(false).when(transactionRepository).existsById(refId);
        final Transaction transaction = new Transaction();
        transaction.setReference(refId);
        transaction.setAccount(new Account(accountId));
        final Account account = new Account(accountId);
        account.setBalance(BigDecimal.ONE);
        final Optional<Account> accountExistence = Optional.of(account);
        doReturn(accountExistence).when(accountRepository).findById(accountId);
        transaction.setAmount(BigDecimal.valueOf(-10));
        try{
            transactionService.createTransaction(transaction);
            fail("Not should reach at this point");
        }catch(final Exception e){
            assertTrue(e instanceof TransactionNotAllowedException);
        }
    }

    @Test
    public void createTransactionMustPersistTransactionInfo() throws Exception{
        final String refId = ANY;
        final String accountId = ACC_IBAN;
        doReturn(false).when(transactionRepository).existsById(refId);
        final Transaction transaction = new Transaction();
        transaction.setReference(refId);
        transaction.setAccount(new Account(accountId));
        final Account account = new Account(accountId);
        account.setBalance(BigDecimal.ONE);
        final Optional<Account> accountExistence = Optional.of(account);
        doReturn(accountExistence).when(accountRepository).findById(accountId);
        transaction.setAmount(BigDecimal.TEN);
        transaction.setFee(BigDecimal.ZERO);
        transactionService.createTransaction(transaction);

        final Account accountToSave = new Account(accountId);
        accountToSave.setBalance(BigDecimal.ONE.add(BigDecimal.TEN));

        final Transaction transactionToSave = new Transaction();
        transactionToSave.setAccount(accountToSave);
        transactionToSave.setAmount(BigDecimal.TEN);
        transactionToSave.setReference(refId);

        //TODO
        verify(accountRepository, times(1)).save(Mockito.argThat(new ArgumentMatcher<Account>(){
            @Override
            public boolean matches(final Account argument){
                return argument.getAccountIban().equals(accountToSave.getAccountIban())
                        && argument.getBalance().equals(accountToSave.getBalance());

            }
        }));
        verify(transactionRepository, times(1)).save(Mockito.argThat(new ArgumentMatcher<Transaction>(){
            @Override
            public boolean matches(final Transaction argument){
                return argument.getAmount().equals(transactionToSave.getAmount())
                        && argument.getReference().equals(transactionToSave.getReference());

            }
        }));

    }

}
