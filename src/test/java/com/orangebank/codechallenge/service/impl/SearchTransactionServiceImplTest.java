package com.orangebank.codechallenge.service.impl;

import static org.hamcrest.Matchers.is;
import static org.junit.Assert.assertThat;
import static org.mockito.Mockito.doReturn;
import static org.mockito.Mockito.mock;

import java.util.Arrays;
import java.util.List;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;
import org.springframework.data.domain.Sort;
import org.springframework.data.domain.Sort.Direction;

import com.orangebank.codechallenge.model.SortEnum;
import com.orangebank.codechallenge.model.persistence.Transaction;
import com.orangebank.codechallenge.repository.AccountRepository;
import com.orangebank.codechallenge.repository.SearchTransactionsRepository;

@RunWith(MockitoJUnitRunner.class)
public class SearchTransactionServiceImplTest{
    private static final String ANY = "ANY";
    private static final String ACC_IBAN = "accIban";
    @Mock
    private AccountRepository accountRepository;
    @Mock
    private SearchTransactionsRepository transactionRepository;
    @InjectMocks
    private SearchTransactionServiceImpl searchTransactionService;

    @Test
    public void testSearchTransactionsWhenOrderSpecifiedIsNull() throws Exception{
        final String accountIban = ANY;
        final Transaction transactionMock1 = mock(Transaction.class);
        final Transaction transactionMock2 = mock(Transaction.class);
        final List<Transaction> list = Arrays.asList(transactionMock1, transactionMock2);
        doReturn(list).when(transactionRepository).searchTransactions(accountIban, null);
        final List<Transaction> listObtained = searchTransactionService.searchTransactions(accountIban, null);
        assertThat(listObtained, is(list));
    }

    @Test
    public void testSearchTransactionsWhenOrderHasBeenSpecified() throws Exception{
        final String accountIban = ANY;
        final Transaction transactionMock1 = mock(Transaction.class);
        final Transaction transactionMock2 = mock(Transaction.class);
        final List<Transaction> list = Arrays.asList(transactionMock1, transactionMock2);
        doReturn(list).when(transactionRepository).searchTransactions(accountIban,
                new Sort(Direction.ASC, Arrays.asList(SearchTransactionServiceImpl.AMOUNT_COLUMN)));
        final List<Transaction> listObtained = searchTransactionService.searchTransactions(accountIban, SortEnum.ASC);
        assertThat(listObtained, is(listObtained));
    }

}
