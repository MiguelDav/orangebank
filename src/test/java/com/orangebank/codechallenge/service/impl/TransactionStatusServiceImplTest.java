package com.orangebank.codechallenge.service.impl;

import static org.hamcrest.Matchers.is;
import static org.hamcrest.Matchers.nullValue;
import static org.junit.Assert.assertThat;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

import java.math.BigDecimal;
import java.time.LocalDateTime;
import java.util.Arrays;
import java.util.List;
import java.util.Optional;
import java.util.stream.Stream;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;

import com.orangebank.codechallenge.calculation.transactionstatus.TransactionChannelPrototype;
import com.orangebank.codechallenge.model.ChannelEnum;
import com.orangebank.codechallenge.model.TransactionStatus;
import com.orangebank.codechallenge.model.TransactionStatusEnum;
import com.orangebank.codechallenge.model.persistence.Transaction;
import com.orangebank.codechallenge.repository.TransactionRepository;

@RunWith(MockitoJUnitRunner.class)
public class TransactionStatusServiceImplTest{
    private static final String ANY_REF = "ANY";

    @Mock
    private TransactionRepository transactionRepository;

    @Mock
    List<TransactionChannelPrototype> transactionChannels;

    @InjectMocks
    private TransactionStatusServiceImpl transactionStatusService;

    @Test
    public void testTransactionStatusINVALID() throws Exception{
        //given
        final Optional<Transaction> optional = Optional.empty();
        when(transactionRepository.findById(ANY_REF)).thenReturn(optional);
        
        
        final TransactionChannelPrototype transactionChannelPrototype = mock(TransactionChannelPrototype.class);
        when(transactionChannelPrototype.getChannel()).thenReturn(ChannelEnum.CLIENT);
        

        final Stream<TransactionChannelPrototype> transactionChannelStream = Arrays.asList(transactionChannelPrototype)
                .stream();
        when(transactionChannels.stream()).thenReturn(transactionChannelStream);

        //when
        final TransactionStatus transactionStatus = transactionStatusService.getTransactionStatus(ANY_REF,
                ChannelEnum.CLIENT);

        //then
        assertThat(transactionStatus.getStatus(), is(TransactionStatusEnum.INVALID));

    }

    @Test
    public void testTransactionStatusWhenTransactionFoundAndSubtractFee() throws Exception{
        //given        
        final Boolean subtractFeeInAmount = Boolean.TRUE;
        final ChannelEnum channelEnum = ChannelEnum.CLIENT;
        final TransactionStatusEnum transactionStatusEnum = TransactionStatusEnum.SETTLED;
        final BigDecimal transactionAmount = BigDecimal.TEN;
        final BigDecimal transactionFee = BigDecimal.ONE;

        mockTransactionChannelsPrototype(subtractFeeInAmount, channelEnum, transactionStatusEnum, transactionAmount,
                transactionFee);

        //when
        final TransactionStatus transactionStatus = transactionStatusService.getTransactionStatus(ANY_REF, channelEnum);


        //then
        assertThat(transactionStatus.getStatus(), is(transactionStatusEnum));
        assertThat(transactionStatus.getFee(), is(nullValue()));
        assertThat(transactionStatus.getAmount(), is(transactionAmount.subtract(transactionFee)));

    }

    @Test
    public void testTransactionStatusWhenTransactionFoundAndNotSubtractFee() throws Exception{
        //given        
        final Boolean subtractFeeInAmount = Boolean.FALSE;
        final ChannelEnum channelEnum = ChannelEnum.CLIENT;
        final TransactionStatusEnum transactionStatusEnum = TransactionStatusEnum.FUTURE;
        final BigDecimal transactionAmount = BigDecimal.TEN;
        final BigDecimal transactionFee = BigDecimal.ONE;

        mockTransactionChannelsPrototype(subtractFeeInAmount, channelEnum, transactionStatusEnum, transactionAmount,
                transactionFee);

        //when
        final TransactionStatus transactionStatus = transactionStatusService.getTransactionStatus(ANY_REF, channelEnum);

        //then
        assertThat(transactionStatus.getStatus(), is(transactionStatusEnum));
        assertThat(transactionStatus.getFee(), is(transactionFee));
        assertThat(transactionStatus.getAmount(), is(transactionAmount));

    }
    /*
    @Test
    public void testTransactionStatusForCLIENTChannelAndTransactionBeforeToday() throws Exception{
        testGetTransactionStatusForCLIENTchannelOrATMBeforeToday(ChannelEnum.CLIENT);
    }

    @Test
    public void testTransactionStatusForATMChannelAndTransactionBeforeToday() throws Exception{
        testGetTransactionStatusForCLIENTchannelOrATMBeforeToday(ChannelEnum.ATM);
    }


    private void testGetTransactionStatusForCLIENTchannelOrATMBeforeToday(final ChannelEnum channelEnum){
        final LocalDateTime localDateTime = LocalDateTime.now();
        when(dateTimeProvider.isBeforeToday(localDateTime)).thenReturn(true);
        final BigDecimal transactionAmount = BigDecimal.TEN;
        final BigDecimal transactionFee = BigDecimal.ONE;

        final BigDecimal transactionStatusAmount = BigDecimal.TEN.subtract(BigDecimal.ONE);
        final BigDecimal transactionStatusFee = null;
        final TransactionStatusEnum transactionStatusEnum = TransactionStatusEnum.SETTLED;

        testGetTransactionStatusWithChannelAndDateAndAmountAndExpectedResult(channelEnum, localDateTime,
                transactionAmount, transactionFee, transactionStatusEnum, transactionStatusAmount,
                transactionStatusFee);
    }
    /*
    @Test
    public void testTransactionStatusForINTERNALChannelAndTransactionBeforeToday() throws Exception{
        final ChannelEnum channelEnum = ChannelEnum.INTERNAL;
        final LocalDateTime localDateTime = LocalDateTime.now();
        when(dateTimeProvider.isBeforeToday(localDateTime)).thenReturn(true);
        final BigDecimal transactionAmount = BigDecimal.TEN;
        final BigDecimal transactionFee = BigDecimal.ONE;

        final BigDecimal transactionStatusAmount = BigDecimal.TEN;
        final BigDecimal transactionStatusFee = BigDecimal.ONE;
        final TransactionStatusEnum transactionStatusEnum = TransactionStatusEnum.SETTLED;

        testGetTransactionStatusWithChannelAndDateAndAmountAndExpectedResult(channelEnum, localDateTime,
                transactionAmount, transactionFee, transactionStatusEnum, transactionStatusAmount,
                transactionStatusFee);
    }

    @Test
    public void testTransactionStatusForCLIENTChannelAndTransactionEqualsToToday() throws Exception{
        testGetTransactionStatusForCLIENTchannelOrATMEqualsToToday(ChannelEnum.CLIENT);
    }

    @Test
    public void testTransactionStatusForATMChannelAndTransactionEqualsToToday() throws Exception{
        testGetTransactionStatusForCLIENTchannelOrATMEqualsToToday(ChannelEnum.ATM);
    }

    private void testGetTransactionStatusForCLIENTchannelOrATMEqualsToToday(final ChannelEnum channelEnum){
        final LocalDateTime localDateTime = LocalDateTime.now();
        when(dateTimeProvider.isBeforeToday(localDateTime)).thenReturn(false);
        when(dateTimeProvider.isEqualsToToday(localDateTime)).thenReturn(true);
        final BigDecimal transactionAmount = BigDecimal.TEN;
        final BigDecimal transactionFee = BigDecimal.ONE;

        final BigDecimal transactionStatusAmount = BigDecimal.TEN.subtract(BigDecimal.ONE);
        final BigDecimal transactionStatusFee = null;
        final TransactionStatusEnum transactionStatusEnum = TransactionStatusEnum.PENDING;

        testGetTransactionStatusWithChannelAndDateAndAmountAndExpectedResult(channelEnum, localDateTime,
                transactionAmount, transactionFee, transactionStatusEnum, transactionStatusAmount,
                transactionStatusFee);
    }

    @Test
    public void testTransactionStatusForINTERNALChannelAndTransactionEqualstToToday() throws Exception{
        final ChannelEnum channelEnum = ChannelEnum.INTERNAL;
        final LocalDateTime localDateTime = LocalDateTime.now();
        when(dateTimeProvider.isBeforeToday(localDateTime)).thenReturn(false);
        when(dateTimeProvider.isEqualsToToday(localDateTime)).thenReturn(true);
        final BigDecimal transactionAmount = BigDecimal.TEN;
        final BigDecimal transactionFee = BigDecimal.ONE;

        final BigDecimal transactionStatusAmount = BigDecimal.TEN;
        final BigDecimal transactionStatusFee = BigDecimal.ONE;
        final TransactionStatusEnum transactionStatusEnum = TransactionStatusEnum.PENDING;

        testGetTransactionStatusWithChannelAndDateAndAmountAndExpectedResult(channelEnum, localDateTime,
                transactionAmount, transactionFee, transactionStatusEnum, transactionStatusAmount,
                transactionStatusFee);
    }

    private void testGetTransactionStatusWithChannelAndDateAndAmountAndExpectedResult(final ChannelEnum channelEnum,
            final LocalDateTime localDateTime, final BigDecimal transactionAmount, final BigDecimal transactionFee,
            final TransactionStatusEnum transactionStatusEnum, final BigDecimal transactionStatusAmount,
            final BigDecimal transactionStatusFee){

        //given
        final Transaction transaction = mock(Transaction.class);
        when(transaction.getDate()).thenReturn(localDateTime);
        when(transaction.getAmount()).thenReturn(transactionAmount);
        when(transaction.getFee()).thenReturn(transactionFee);
        final Optional<Transaction> optional = Optional.of(transaction);
        when(transactionRepository.findById(ANY_REF)).thenReturn(optional);

        //when
        final TransactionStatus transactionStatus = transactionStatusService.getTransactionStatus(ANY_REF, channelEnum);

        //then
        assertThat(transactionStatus.getAmount(), is(transactionStatusAmount));
        assertThat(transactionStatus.getFee(), is(transactionStatusFee));
        assertThat(transactionStatus.getReference(), is(ANY_REF));
        assertThat(transactionStatus.getStatus(), is(transactionStatusEnum));

    }

    @Test
    public void testTransactionStatusForCLIENTChannelAndTransactionGreaterThanToday() throws Exception{
        final ChannelEnum channelEnum = ChannelEnum.CLIENT;
        final LocalDateTime localDateTime = LocalDateTime.now();
        when(dateTimeProvider.isBeforeToday(localDateTime)).thenReturn(false);
        when(dateTimeProvider.isEqualsToToday(localDateTime)).thenReturn(false);
        final BigDecimal transactionAmount = BigDecimal.TEN;
        final BigDecimal transactionFee = BigDecimal.ONE;

        final BigDecimal transactionStatusAmount = BigDecimal.TEN.subtract(BigDecimal.ONE);
        final BigDecimal transactionStatusFee = null;
        final TransactionStatusEnum transactionStatusEnum = TransactionStatusEnum.FUTURE;

        testGetTransactionStatusWithChannelAndDateAndAmountAndExpectedResult(channelEnum, localDateTime,
                transactionAmount, transactionFee, transactionStatusEnum, transactionStatusAmount,
                transactionStatusFee);
    }

    @Test
    public void testTransactionStatusForATMChannelAndTransactionGreaterThanToday() throws Exception{
        final ChannelEnum channelEnum = ChannelEnum.ATM;
        final LocalDateTime localDateTime = LocalDateTime.now();
        when(dateTimeProvider.isBeforeToday(localDateTime)).thenReturn(false);
        when(dateTimeProvider.isEqualsToToday(localDateTime)).thenReturn(false);
        final BigDecimal transactionAmount = BigDecimal.TEN;
        final BigDecimal transactionFee = BigDecimal.ONE;

        final BigDecimal transactionStatusAmount = BigDecimal.TEN.subtract(BigDecimal.ONE);
        final BigDecimal transactionStatusFee = null;
        final TransactionStatusEnum transactionStatusEnum = TransactionStatusEnum.PENDING;

        testGetTransactionStatusWithChannelAndDateAndAmountAndExpectedResult(channelEnum, localDateTime,
                transactionAmount, transactionFee, transactionStatusEnum, transactionStatusAmount,
                transactionStatusFee);
    }


    @Test
    public void testTransactionStatusForINTERNALChannelAndTransactionGreatherThanToday() throws Exception{
        final ChannelEnum channelEnum = ChannelEnum.INTERNAL;
        final LocalDateTime localDateTime = LocalDateTime.now();
        when(dateTimeProvider.isBeforeToday(localDateTime)).thenReturn(false);
        when(dateTimeProvider.isEqualsToToday(localDateTime)).thenReturn(false);
        final BigDecimal transactionAmount = BigDecimal.TEN;
        final BigDecimal transactionFee = BigDecimal.ONE;

        final BigDecimal transactionStatusAmount = BigDecimal.TEN;
        final BigDecimal transactionStatusFee = BigDecimal.ONE;
        final TransactionStatusEnum transactionStatusEnum = TransactionStatusEnum.FUTURE;

        testGetTransactionStatusWithChannelAndDateAndAmountAndExpectedResult(channelEnum, localDateTime,
                transactionAmount, transactionFee, transactionStatusEnum, transactionStatusAmount,
                transactionStatusFee);
    }
     */

    private void mockTransactionChannelsPrototype(final Boolean subtractFeeInAmount, final ChannelEnum channelEnum,
            final TransactionStatusEnum transactionStatusEnum, final BigDecimal transactionAmount,
            final BigDecimal transactionFee){
        final LocalDateTime localDateTime = LocalDateTime.now();
        final Transaction transaction = mock(Transaction.class);
        when(transaction.getAmount()).thenReturn(transactionAmount);
        when(transaction.getFee()).thenReturn(transactionFee);
        when(transaction.getDate()).thenReturn(localDateTime);
        final Optional<Transaction> optional = Optional.of(transaction);
        when(transactionRepository.findById(ANY_REF)).thenReturn(optional);
        final TransactionChannelPrototype transactionChannelPrototype = mock(TransactionChannelPrototype.class);
        when(transactionChannelPrototype.getChannel()).thenReturn(channelEnum);
        when(transactionChannelPrototype.getStatusWithDateTime(localDateTime)).thenReturn(transactionStatusEnum);

        when(transactionChannelPrototype.subtractFeesInAmount()).thenReturn(subtractFeeInAmount);

        final Stream<TransactionChannelPrototype> transactionChannelStream = Arrays.asList(transactionChannelPrototype)
                .stream();
        when(transactionChannels.stream()).thenReturn(transactionChannelStream);
    }
}
