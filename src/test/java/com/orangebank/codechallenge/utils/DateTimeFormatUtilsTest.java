package com.orangebank.codechallenge.utils;

import static org.hamcrest.Matchers.is;
import static org.hamcrest.Matchers.notNullValue;
import static org.hamcrest.Matchers.nullValue;
import static org.junit.Assert.assertThat;
import static org.junit.Assert.assertTrue;

import java.time.LocalDateTime;
import java.util.TimeZone;

import org.junit.Test;

/**
 * 
 * Utility class to format/parse LocalDateTimes and Strings
 *
 */
public class DateTimeFormatUtilsTest{

    DateTimeFormatUtils dateTimeFormatUtils = new DateTimeFormatUtils();
    private static final String DATE_TIME_USED = "2014-12-22T10:15:30.001Z";

    @Test
    public void testGetNowAsLocalDateTime() throws Exception{
        //given
        final LocalDateTime now = LocalDateTime.now();
        //when
        final LocalDateTime localDateTime = dateTimeFormatUtils.getNowAsLocalDateTime();
        //then
        assertThat(localDateTime, is(notNullValue()));
        assertTrue(now.minusHours(12).isBefore(localDateTime));
    }

    @Test
    public void testParseToLocalDateTime() throws Exception{

        final LocalDateTime localDateTimeParsed = dateTimeFormatUtils.parseToLocalDateTime(DATE_TIME_USED);
        final LocalDateTime localDateTime = localDateTimeParsed
                .plusSeconds(TimeZone.getDefault().getRawOffset() / 1000 * -1);
        assertThat(localDateTime.getHour(), is(10));
        assertThat(localDateTime.getMonthValue(), is(12));
        assertThat(localDateTime.getYear(), is(2014));
        assertThat(localDateTime.getDayOfMonth(), is(22));
        assertThat(localDateTime.getMinute(), is(15));
        assertThat(localDateTime.getSecond(), is(30));
    }

    @Test
    public void testParseToLocalDateTimeWithNulls() throws Exception{
        assertThat(dateTimeFormatUtils.parseToLocalDateTime(null), is(nullValue()));

    }

    @Test
    public void testFormatFromLocalDateTime() throws Exception{
        final LocalDateTime localDateTimeParsed = dateTimeFormatUtils.parseToLocalDateTime(DATE_TIME_USED);
        final String dateFormatted = dateTimeFormatUtils.formatFromLocalDateTime(localDateTimeParsed);

        assertThat(dateFormatted, is(DATE_TIME_USED));
    }

    @Test
    public void testFormatFromLocalDateTimeWithNulls() throws Exception{
        assertThat(dateTimeFormatUtils.formatFromLocalDateTime(null), is(nullValue()));
    }

}
