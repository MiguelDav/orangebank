package com.orangebank.codechallenge.utils;

import static org.hamcrest.Matchers.is;
import static org.junit.Assert.assertThat;

import java.time.LocalDateTime;

import org.junit.Test;

public class LocalDateTimeComparationUtilsTest{

    LocalDateTimeComparationUtils dateTimeUtils = new LocalDateTimeComparationUtils();



    @Test
    public void testIsBeforeToday() throws Exception{
        final LocalDateTime yesterday = LocalDateTime.now().minusDays(1);
        final LocalDateTime today = LocalDateTime.now();

        final LocalDateTime tomorrow = LocalDateTime.now().plusDays(1);

        assertThat(dateTimeUtils.isBeforeToday(yesterday), is(true));
        assertThat(dateTimeUtils.isBeforeToday(today), is(false));
        assertThat(dateTimeUtils.isBeforeToday(tomorrow), is(false));
    }

    @Test
    public void testIsEqualsToToday() throws Exception{
        final LocalDateTime yesterday = LocalDateTime.now().minusDays(1);
        final LocalDateTime today = LocalDateTime.now();

        final LocalDateTime tomorrow = LocalDateTime.now().plusDays(1);

        assertThat(dateTimeUtils.isEqualsToToday(yesterday), is(false));
        assertThat(dateTimeUtils.isEqualsToToday(today), is(true));
        assertThat(dateTimeUtils.isEqualsToToday(tomorrow), is(false));
    }

}
