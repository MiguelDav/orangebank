Feature: Create Transaction Functionality


Scenario: Add one transaction for an account that does not exist in the system
	Given An Acount that does not exist in the system
	When I add one transaction with ref XXXX in the system
	Then the system should return http status 412
	
Scenario: Add one transaction that exist in the system
	Given A transaction XXXX that exist in our system
	When I add one transaction with ref XXXX in the system
	Then the system should return http status 409
	
Scenario: Add one transaction that leaves the total account balance bellow 0 because of amount value
  Given An Account AAAAAA that exists in the system
  And account has balance of 30.00
	When I add one transaction with ref XXXAAA1 for Account AAAAAA with amount=-33.20 and fee=1.20 
	Then the system should return http status 403
	
Scenario: Add one transaction that leaves the total account balance bellow 0 because of fee value
  Given An Account AAAAAA that exists in the system
  And account has balance of 2.00
	When I add one transaction with ref XXXAAA1 for Account AAAAAA with amount=1.20 and fee=9.20 
	Then the system should return http status 403 	

Scenario: Add one transaction int our system sucessfully
  Given An Account AAAAAA2 that exists in the system
	When I add one transaction with ref XXXAAA2 for Account AAAAAA2 with amount=3.20 and fee=0.20 and description='This is a description' and date='2019-07-16T16:55:42.001Z'
	Then the system should return http status 200
	And the transaction XXXAAA2 for Account AAAAAA2 exists in the system with amount=3.20 and fee=0.20 and description='This is a description' and date='2019-07-16T16:55:42.001Z'
				
	