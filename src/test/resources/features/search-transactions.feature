Feature: Search Transactions Functionality


Scenario: Search transactions for account that does not exist
  Given An Acount that does not exist in the system
	When I search transactions with null order for Amount		
	Then the system should return http status 404			
		
Scenario: Search transactions in ascending order for Amount
  Given An Account BBBB that exists in the system
  And 5 transactions with random values
	When I search transactions with ASC order for Amount	
	Then there are 5 transactions sorted by Amount in ascending order

Scenario: Search transactions in descending order for Amount
  Given An Account CCCC that exists in the system
  And 5 transactions with random values
	When I search transactions with DESC order for Amount	
	Then there are 5 transactions sorted by Amount in descending order			
	