Feature: Transaction Status Functionality


Scenario: Check status for a transaction that is not stored in our system, It returns status IVALID 
	Given A transaction that is not stored in our system
	When I check the status from ANY channel
	Then The system returns the status 'INVALID'

Scenario: Check status SETTLED for CLIENT channel and past transactions
	Given A transaction that is stored in our system with amount=3.20 and fee=1.20 and transaction date is 'BEFORE' today
	When I check the status from CLIENT channel
	Then The system returns the status 'SETTLED'
	And The amount is 2.00 (substracting the fee)
	
Scenario: Check status SETTLED for ATM channel and past transactions
	Given A transaction that is stored in our system with amount=5.20 and fee=1.20 and transaction date is 'BEFORE' today
	When I check the status from ATM channel
	Then The system returns the status 'SETTLED'
	And The amount is 4.00 (substracting the fee)
	
Scenario: Check status SETTLED for INTERNAL channel channel and past transactions
	Given A transaction that is stored in our system with amount=3.20 and fee=1.20 and transaction date is 'BEFORE' today
	When I check the status from INTERNAL channel
	Then The system returns the status 'SETTLED'
	And The amount is 3.20
	And The fee is 1.20
	
Scenario: Check status PENDING for CLIENT channel and current transactions
	Given A transaction that is stored in our system with amount=3.20 and fee=1.20 and transaction date is 'EQUALS TO' today
	When I check the status from CLIENT channel
	Then The system returns the status 'PENDING'	
	And The amount is 2.00 (substracting the fee)
	
Scenario: Check status PENDING for ATM channel and current transactions
	Given A transaction that is stored in our system with amount=3.20 and fee=1.20 and transaction date is 'EQUALS TO' today
	When I check the status from CLIENT channel
	Then The system returns the status 'PENDING'	
	And The amount is 2.00 (substracting the fee)

Scenario: Check status PENDING for INTERNAL channel and current transactions
	Given A transaction that is stored in our system with amount=3.20 and fee=1.20 and transaction date is 'EQUALS TO' today
	When I check the status from INTERNAL channel
	Then The system returns the status 'PENDING'	
	And The amount is 3.20
	And The fee is 1.20	

Scenario: Check status FUTURE for CLIENT channel and future transactions
	Given A transaction that is stored in our system with amount=3.20 and fee=1.20 and transaction date is 'GREATER THAN' today
	When I check the status from CLIENT channel
	Then The system returns the status 'FUTURE'		
	And The amount is 2.00 (substracting the fee)
	
Scenario: Check status PENDING for ATM channel and future transactions
	Given A transaction that is stored in our system with amount=3.20 and fee=1.20 and transaction date is 'GREATER THAN' today
	When I check the status from ATM channel
	Then The system returns the status 'PENDING'	
	And The amount is 2.00 (substracting the fee)

Scenario: Check status FUTURE for INTERNAL channel and future transactions
	Given A transaction that is stored in our system with amount=3.20 and fee=1.20 and transaction date is 'GREATER THAN' today
	When I check the status from INTERNAL channel
	Then The system returns the status 'FUTURE'	
	And The amount is 3.20
	And The fee is 1.20	
